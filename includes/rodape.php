<div id="content_rodape">
    <div id="rodape">
      <a href="#" class="scrollup">scrollup</a>
        <a href="<?php echo Util::caminho_projeto(); ?>/" title="Só Bandeiras">
            <div class="logo">&nbsp;</div>
        </a>

        <ul>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/" title="HOME">HOME</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/empresa/" title="EMPRESA">EMPRESA</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/" title="PRODUTOS">PRODUTOS</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/servicos/" title="SERVIÇOS">SERVIÇOS</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/parceiros/" title="PARCEIROS">PARCEIROS</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/contato" title="CONTATOS">CONTATOS</a></li>
        </ul>


            <div id="barra_info1">

                <div class="telefone">
                    <p><?php Util::imprime($config[estado1]); ?></p>
                    <div href="tel:+55<?php Util::imprime($config[telefone1]); ?>">
                        <h5><i class="fa fa-phone right10"></i><?php Util::imprime($config[telefone1]); ?></h5>
                    </div>
                </div>

                <?php if(!empty($config[telefone2])): ?>
                <div class="telefone">
                    <p><?php Util::imprime($config[estado2]); ?></p>
                    <div href="tel:+55<?php Util::imprime($config[telefone2]); ?>">
                        <h5><?php Util::imprime($config[telefone2]); ?></h5>
                    </div>
                </div>
                <?php endif; ?>

                <?php if(!empty($config[telefone3])): ?>
                <div class="telefone">
                    <p><?php Util::imprime($config[estado3]); ?></p>
                    <div href="tel:+55<?php Util::imprime($config[telefone3]); ?>">
                        <h5><?php Util::imprime($config[telefone3]); ?></h5>
                    </div>
                </div>
                <?php endif; ?>




            </div>

            <div class="endereco_rodape">
              <div href="<?php Util::imprime($config[endereco1]); ?>">
                  <h2><i class="fa fa-home right10"></i><?php Util::imprime($config[endereco1]); ?></h2>
              </div>

              <?php if(!empty($config[endereco2])): ?>
                <div href="<?php Util::imprime($config[endereco2]); ?>">
                    <h2><i class="fa fa-home right10"></i><?php Util::imprime($config[endereco2]); ?></h2>
                </div>
              <?php endif; ?>

              <?php if(!empty($config[endereco3])): ?>
                <div href="<?php Util::imprime($config[endereco3]); ?>">
                    <h2><i class="fa fa-home right10"></i><?php Util::imprime($config[endereco3]); ?></h2>
                </div>
              <?php endif; ?>
            </div>







         <?php if ($config[google_plus] != "") { ?>
            <a class="pull-left top40 left50 plus" style="float: right; margin-top: -60px; color: #fff; margin-right: 95px;" href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
                <p class=""><i class="fa fa-google-plus pt25"></i></p>
            </a>
        <?php } ?>


        <a href="http://www.homewebbrasil.com.br/" title="Homeweb Marketing" target="_blank">
            <div class="logo_homeweb">&nbsp;</div>
        </a>

    </div>
</div>

<div class="container ">
<div class="row text-center">
  <p class="copyright top10">TODOS OS DIREITOS RESERVADOS.</p>
</div>
</div>
