<script>
$(function() {
    $("#btn_filtro_marca").click(function() {
        $("#botoes_filtros a").removeClass("active");
        $("#btn_filtro_marca").addClass("active");
        $("#filtro_categoria").slideUp("fast");
        $("#filtro_marca").slideToggle();
    });


});
</script>

<form name="form_busca" id="form_busca" action="<?php echo Util::caminho_projeto(); ?>/servicos/" method="post">
    <input type="text" name="busca" id="busca" placeholder="Encontre o serviço desejado" />
    <input type="submit" id="enviar" value="Enviar" />
</form>

<div id="content_filtros">

    <!-- FILTRO DE CATEGORIA -->
    <div id="filtro_categoria">
        <h1>SELECIONE A CATEGORIA:</h1>
        <ul>
            <?php
            $result = $obj_site->select("tb_categoria_servicos");
            if(mysql_num_rows($result) == 0){
              echo "<div class='col-xs-12 top10 bottom10'> <p class='bg-danger' style='padding: 20px;'>Nenhum registro encontrado.</p> </div>";

            }else{
              while($row = mysql_fetch_array($result)){
                    ?>
                    <li>
                        <a href="<?php echo Util::caminho_projeto(); ?>/servicos/categoria/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                            <img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
                        </a>
                        <p>
                            <a href="<?php echo Util::caminho_projeto(); ?>/servicos/categoria/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                                <?php //Util::imprime($row[titulo]); ?>
                            </a>
                        <p>
                    </li>
                    <?php
                    if ($i == 2) {
                      echo '<div class="clearfix"></div>';
                      $i = 0;
                    }else{
                      $i++;
                    }
                  }
                }
                ?>
        </ul>
    </div><!-- FIM FILTRO DE CATEGORIA -->


    <!-- BOTÕES FILTROS -->
    <div id="botoes_filtros">
        <h5>FILTRAR POR</h5>
        <a href="<?php echo Util::caminho_projeto(); ?>/servicos/" title="TODOS OS SERVIÇOS">TODOS</a>
        <a href="<?php echo Util::caminho_projeto(); ?>/servicos/categoria/" title="FILTRAR POR CATEGORIA">CATEGORIA</a>
    </div><!-- FIM BOTÕES FILTROS -->

</div>
