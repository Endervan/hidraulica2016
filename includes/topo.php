<div id="content_topo">
    <div id="topo">

        <a href="<?php echo Util::caminho_projeto(); ?>/" title="Só Bandeiras">
            <div class="logo">&nbsp;</div>
        </a>

        <ul id="menu">
            <li class="<?php echo $class_home ?>"><a href="<?php echo Util::caminho_projeto(); ?>/" title="HOME" class="home">HOME</a></li>
            <li class="<?php echo $class_empresa ?>"><a href="<?php echo Util::caminho_projeto(); ?>/empresa/" title="EMPRESA" class="empresa">EMPRESA</a></li>
            <li class="<?php echo $class_produtos ?>"><a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/" title="PRODUTOS" class="produtos">PRODUTOS</a></li>
            <li class="<?php echo $class_servicos ?>"><a href="<?php echo Util::caminho_projeto(); ?>/servicos/categoria" title="SERVIÇOS" class="servicos">SERVIÇOS</a></li>
            <li class="<?php echo $class_parceiros ?>"><a href="<?php echo Util::caminho_projeto(); ?>/parceiros/" title="parceiros" class="disklimpeza">PARCEIROS</a></li>
            <li class="<?php echo $class_contato ?>"><a href="<?php echo Util::caminho_projeto(); ?>/contato" title="CONTATOS" class="contatos">CONTATOS</a></li>
            <li class=""><a href="<?php echo Util::caminho_projeto(); ?>/trabalhe-conosco/ficha.php" title="TRABALHE CONOSCO" class="contatos">TRABALHE CONOSCO</a></li>

        </ul>

    </div>
</div>

<div id="content_barra_info">
    <div id="barra_info">

        <div class="telefone">
            <p><?php Util::imprime($config[estado1]); ?></p>
            <div href="<?php Util::imprime($config[telefone1]); ?>">
                <h5><?php Util::imprime($config[telefone1]); ?></h5>
            </div>
        </div>

        <?php if(!empty($config[telefone2])): ?>
        <div class="telefone">
            <p><?php Util::imprime($config[estado2]); ?></p>
            <div href="<?php Util::imprime($config[telefone2]); ?>">
                <h5><?php Util::imprime($config[telefone2]); ?></h5>
            </div>
        </div>
        <?php endif; ?>

        <?php if(!empty($config[telefone3])): ?>
        <div class="telefone">
            <p><?php Util::imprime($config[estado3]); ?></p>
            <div href="<?php Util::imprime($config[telefone3]); ?>">
                <h5><?php Util::imprime($config[telefone3]); ?></h5>
            </div>
        </div>
        <?php endif; ?>

        <a href="<?php echo Util::caminho_projeto(); ?>/orcamento" class="botao" title="ORÇAMENTOS">
            <div class="icon_orcamento">&nbsp;</div>
            ORÇAMENTOS
        </a>

        <a href="<?php echo Util::caminho_projeto(); ?>/contato" class="botao" title="COMO CHEGAR">
            <div class="icon_como_chegar">&nbsp;</div>
            COMO CHEGAR
        </a>

    </div>
</div>
