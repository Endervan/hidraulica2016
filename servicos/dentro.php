<?php
require_once("../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTO
$url = Util::trata_dados_formulario($_REQUEST[get1]);
if(!empty($url))
{
	$complemento = "AND url_amigavel = '".$url."'";
}

$result = $obj_site->select("tb_servicos",$complemento);
$row_servico = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description($row_servico[description_google]);
$keywords = $obj_site->get_keywords($row_servico[keywords_google]);
$titulo_pagina = $obj_site->get_title($row_servico[description_google]);
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>

	<script type="text/javascript">
	function mudaFoto(id)
	{
		var elemento = "#foto_"+id+" img";
		$("#img_principal a img").stop(true, true).fadeOut(500).hide(500);
		$(elemento).stop(true, true).fadeIn(1000).show(500);
	}
	</script>


</head>

<body>

	<?php include("../includes/topo.php"); ?>

	<?php include("../includes/banners_internas.php"); ?>

	<div class="clear">&nbsp;</div>

	<div id="conteudo" class="produtos">

		<?php include("../includes/filtros_servico.php"); ?>

		<div id="content_holder">

			<?php /*
			$result_categoria = $obj_site->select("tb_categoria_servicos","AND idcategoriaservico = ".$row_servico[id_categoriaservico]);
			$row_categoria = mysql_fetch_array($result_categoria);
			*/
			?>

			<div id="galeria_produto">

				<div id="content_imgs_principal">
					<div id="img_principal">
						<?php
						$result = $obj_site->select("tb_galerias_servicos","AND id_servico = '$row_servico[idservico]'");
						if(mysql_num_rows($result) > 0)
						{
							while($row = mysql_fetch_array($result))
							{
								$dados[] = $row;
								?>
								<a href="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[imagem]); ?>" title="<?php Util::imprime($row_produto[titulo]); ?>" data-gallery="1" id="foto_<?php Util::imprime($row[0]); ?>">
									<?php  $obj_site->redimensiona_imagem("../uploads/$row[imagem]",381,281, array('alt'=>$row_servico[titulo])); ?>
								</a>

								<?php
							}
						}
						?>
					</div>
				</div>


				<div id="content_tumbs">

					<a id="prev" class="seta_voltar" href="#">&lt;</a>
					<a id="next" class="seta_avancar" href="#">&gt;</a>

					<ul id="carrousel">

						<?php
						if(count($dados) > 0):

							foreach($dados as $dado):
							?>
								<li>
									<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(<?php echo $dado[0] ?>)">
										<?php $obj_site->redimensiona_imagem("../uploads/tumb_$dado[imagem]", 77, 57, array('alt'=>$row_produto[titulo])); ?>
									</a>
								</li>
							<?php
							endforeach;

						endif;
						?>

					</ul>
				</div>

			</div>

			<div id="content_informacoes_produtos">

				<h2><?php Util::imprime($row_servico[titulo]); ?></h2>
				<?php /*?><p class="preco">R$ <?php echo Util::formata_moeda($row_servico[preco]); ?></p><?php */?>

				<?php echo($row_servico[descricao]); ?>

				<br><br>
				<a href="javascript:void(0);" class="btn-comprar" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row_servico[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row_servico[0]) ?>, 'servico'">
					Adicionar ao orçamento
				</a>

			</div>
		</div>

		<h5 class="top25"><span>VEJA TAMBÉM:</span></h5>
		<ul id="lista_produtos" class="dentro">
			<?php
			$result = $obj_site->select("tb_servicos", "order by rand() limit 4");
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_array($result))
				{

					?>
					<li>
						<a href="<?php echo Util::caminho_projeto(); ?>/servicos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

							<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",200, 180, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
							<p><?php Util::imprime($row[titulo]); ?></p>
						</a>

					</li>
					<?php
				}
			}
			?>
		</ul>

		<div class="clear">&nbsp;</div>

	</div>

	<?php include("../includes/rodape.php"); ?>

</body>
</html>
