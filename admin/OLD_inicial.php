<?php
require_once("../class/Include.class.php");
require_once("trava.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all"/>
<title>Admin - <?php echo $_SERVER['SERVER_NAME'] ?></title>
</head>

<body>
<!-- ============================= CONTAINER =================================== -->
<div id="container">
	
    
    <!-- ============================= CONTAINER LATERAL =================================== -->
    <?php require_once("includes/lateral.php") ?>
    <!-- ============================= CONTAINER LATERAL =================================== -->
    
    
    <!-- ============================= CONTAINER CONTEUDO =================================== -->
    <div id="container-conteudo">
    	
        <!-- ============================= CONTAINER CONTEUDO TOPO =================================== -->
        <?php require_once("includes/topo.php") ?>
        <!-- ============================= CONTAINER CONTEUDO TOPO =================================== -->
        
        
        <!-- ============================= CONTAINER CONTEUDO HOLDER =================================== -->
        <div id="container-conteudo-holder">
        	
            <!-- Acesso rápido -->
            <div class="acesso-rapido">
            	<div class="acesso-rapido-titulo">
                	&nbsp;
                </div>
            	<ul>
                    <li>
                    	<a href="<?php echo Util::caminho_projeto(); ?>/admin/logoff.php" title="Sair">
                        	<img src="imgs/icon-6.jpg" alt="Sair" />
                        </a>
                    </li>
            	</ul>
            </div>
            <!-- Acesso rápido -->
            
            
            
            
        
        </div>
        <!-- ============================= CONTAINER CONTEUDO HOLDER =================================== -->
        
    	
    
    
    
    </div>
    <!-- ============================= CONTAINER CONTEUDO =================================== -->


</div>
<!-- ============================= CONTAINER =================================== -->
</body>
</html>