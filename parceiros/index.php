﻿<?php
require_once("../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description("");
$keywords = $obj_site->get_keywords("");
$titulo_pagina = $obj_site->get_title("");
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>

	<?php include("../includes/banners_internas.php"); ?>

	<div class="clear">&nbsp;</div>

	<div id="conteudo" class="disklimpeza">

		<?php $dados = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
		<h2 class="msg"><?php Util::imprime($dados[descricao],500); ?></h2>

		<div id="lista_parceiros">
			<ul>
				<?php
				$result = $obj_site->select("tb_parceiros");
				if(mysql_num_rows($result) > 0)
				{
					while($row = mysql_fetch_array($result))
					{
						?>
						<li>
								<?php
								if(!empty($row[imagem]))
								{
									?>
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php echo Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
									<?php
								}
								?>
								<h5><?php Util::imprime($row[titulo]); ?></h5>

						</li>
						<?php
					}
				}
				else
				{
					?>
					<h1>Nenhum Produto Encontrado</h1>
					<?php
				}
				?>
			</ul>
		</div>

	</div>

	<?php include("../includes/rodape.php"); ?>

</body>
</html>
