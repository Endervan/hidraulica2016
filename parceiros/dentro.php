﻿<?php
require_once("../class/Include.class.php");
$obj_site = new Site();

// INTERNA DE DICAS
$url = Util::trata_dados_formulario($_REQUEST[get1]);
if(!empty($url))
{
	$complemento = "AND url_amigavel = '".$url."'";
}

$result = $obj_site->select("tb_servicos",$complemento);
$row_disklimpeza = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description($row_disklimpeza[description_google]);
$keywords = $obj_site->get_keywords($row_disklimpeza[keywords_google]);
$titulo_pagina = $obj_site->get_title($row_disklimpeza[description_google]);
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>

	<script>
		$(document).ready(function(){
			$("#slider_portifolio").easySlider({
				auto: true,
				continuous: true
			});
		});
	</script>


</head>

<body>

	<?php include("../includes/topo.php"); ?>

	<?php include("../includes/banners_internas.php"); ?>

	<div class="clear">&nbsp;</div>

	<div id="conteudo" class="dicas">

	

		<div id="content_holder">
			<div id="content_principal" class="conteudo-portfolio">
				<p class="data"><?php echo Util::formata_data($row_disklimpeza[data]); ?></p>
				<h1><?php Util::imprime($row_disklimpeza[titulo]) ?></h1>


				<?php Util::imprime($row_disklimpeza[descricao]); ?>





				<div id="slider_portifolio">
					<ul>
						<?php
						$result = $obj_site->select("tb_galerias_servicos","AND id_servico = '$row_disklimpeza[0]'");
						if(mysql_num_rows($result) > 0){
							while($row = mysql_fetch_array($result)){
							?>
								<li>
									<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 700, 550); ?>
								</li>
							<?php
							}
						}
						?>
					</ul>
				</div>

			</div>

			<div id="content_auxiliar">

				<div class="titulos">
					<h3>NOSSOS DESTAQUES DE</h3>
					<h2>PRODUTOS</h2>
				</div>

				<div class="clear">&nbsp;</div>

				<ul>
					<?php
					$result = $obj_site->select("tb_produtos","ORDER BY RAND() LIMIT 3");
					if(mysql_num_rows($result) > 0)
					{
						while($row = mysql_fetch_array($result))
						{
							?>
							<li>
								<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
									<p><?php Util::imprime($row[titulo]); ?></p>
								</a>
								<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="leia_mais" title="VER MAIS">
									VER MAIS
								</a>
							</li>
							<?php
						}
					}
					?>

				</ul>


			</div>
		</div>

		<div class="clear">&nbsp;</div>

	</div>

	<?php include("../includes/rodape.php"); ?>

</body>
</html>
