﻿<?php
require_once("../class/Include.class.php"); 
require_once('../class/recaptchalib.php');
$obj_site = new Site();




?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="contato">
		
		<div class="titulos">
			<h3>CONTATOS</h3>
			<h2>FALE CONOSCO</h2>
		</div>
		
		<div class="content_navegacao_forms">
			<ul>
				<li><a href="<?php echo Util::caminho_projeto() ?>/contato/" title="FALE CONOSCO" class="fale_conosco">FALE CONOSCO</a></li>
				<li><a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco/ficha.php" title="TRABALHE CONOSCO" class="trabalhe_conosco">TRABALHE CONOSCO</a></li>
				<li><a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ORÇAMENTOS" class="orcamentos">ORÇAMENTOS</a></li>
			</ul>
		</div>
		
		<div class="clear">&nbsp;</div>
		
		<?php  
		//	VERIFICO SE E PARA ENVIAR O EMAIL
		if(isset($_POST[nome]))
		{
			
					$nome_remetente = Util::trata_dados_formulario( utf8_encode( $_POST[nome] ) );
					$assunto = "Contato pelo site $_SERVER[SERVER_NAME]";
					$email = Util::trata_dados_formulario($_POST[email]);
					$mensagem = (nl2br(addslashes(trim($_POST[mensagem]))));
					$telefone = Util::trata_dados_formulario($_POST[telefone]);
					
					$texto_mensagem = "
									  Nome: $nome_remetente <br />
									  Assunto: $assunto <br />
									  Telefone: $telefone <br />
									  Email: $email <br />
									  Mensagem:	<br />
									  $mensagem
									  ";
					
					if(Util::envia_email($config[email], "Contato pelo site $_SERVER[SERVER_NAME]", $texto_mensagem, $nome_remetente, $email));
                        Util::script_msg("Obrigado por entrar em contato.");
                                        
                                        /*
                                        if(Util::envia_email($config[email], "Contato pelo site $_SERVER[SERVER_NAME]", $texto_mensagem, $nome_remetente, $email))
					{
						Util::script_msg("Obrigado por entrar em contato.");
					}
					else
					{
						Util::script_msg("Houve um erro ao enviar o email.");
					}
                                         * 
                                         */
		}
		?>
		
		<form id="form_contato" name="form_contato" method="post" action="#">
            
			<input type="text" name="nome" id="nome" title="Campo Nome" placeholder="SEU NOME" required="required" />
			<input type="text" name="email" id="email" title="Campo E-mail" placeholder="SEU E-MAIL" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" />
			<input type="text" name="telefone" id="telefone" title="Campo Telefone" placeholder="SEU TELEFONE" required="required" />
			
			<textarea name="mensagem" rows="5" id="mensagem" title="Campo Mensagem" placeholder="SUA MENSAGEM" required="required"></textarea>
			
			<div style="float:left;">
			</div>
			
			<input type="submit" name="enviar" id="enviar" value="Enviar" />
		</form>
		
		
		
		<div class="content_mapa">
			<h2 class="titulo_mapa">SAIBA COMO CHEGAR</h2>
			
			<iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="310" frameborder="0" style="border:0" allowfullscreen></iframe>

			<?php if (!empty($config[src_place_2])): ?>
				<br><br>
				<iframe src="<?php Util::imprime($config[src_place_2]); ?>" width="100%" height="310" frameborder="0" style="border:0" allowfullscreen></iframe>	
			<?php endif ?>


			<?php if (!empty($config[src_place_3])): ?>
				<br><br>
				<iframe src="<?php Util::imprime($config[src_place_3]); ?>" width="100%" height="310" frameborder="0" style="border:0" allowfullscreen></iframe>	
			<?php endif ?>
			
			
		</div>
		
		
		
		
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>
