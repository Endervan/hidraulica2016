<?php
require_once("class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description("");
$keywords = $obj_site->get_keywords("");
$titulo_pagina = $obj_site->get_title("");

?>
<!doctype html>
<html>
<head>
	<?php include("includes/head.php"); ?>

	<!-- slider css -->
	<style>

	#slider .rsOverflow, #slider .rsABlock, #slider .rsSlide {overflow:visible !important;}

	#slider .rsTabs { width:925px; padding:0 38px; margin-top:-30px}
	#slider .rsTab {display:block; width:211px; padding:0; margin:0 10px; height:90px; float:left; background:url(<?php echo Util::caminho_projeto(); ?>/images/btn_slider.png) center center no-repeat; border:none;}
	#slider .rsTab.rsNavSelected {background:url(<?php echo Util::caminho_projeto(); ?>/images/btn_slider_hover.png) center center no-repeat; text-shadow:none; box-shadow:none;}
	#slider .rsTab:active {box-shadow:none;}
	#slider .rsTab .rsTmb{color:#0072bd; font-family: 'cabinbold', sans-serif; font-size:18px; vertical-align:middle; display:table-cell; text-align:center; height:70px; width:200px;}



	.rsContent {color: #FFF; font-size: 24px; line-height: 32px; float: left; }
	.bContainer { position: relative; }
	.rsABlock { position: relative; display: block; left: auto; top: auto; }

	.blockHeadline { font-size: 42px; line-height: 50px; }
	.blockSubHeadline { font-size: 32px; line-height: 40px }
	.txtCent { text-align: center;  width: 100%; }


	@media screen and (min-width: 0px) and (max-width: 960px) {
		.rsContent { font-size: 22px; line-height: 28px; }
		.blockHeadline { font-size: 32px; line-height: 32px;}
		.blockSubHeadline { font-size: 26px; line-height: 32px }
	}

	@media screen and (min-width: 0px) and (max-width: 500px) {
		.royalSlider, .rsOverflow { height: 330px !important; }
		.rsContent { font-size: 18px; line-height: 26px; }
		.blockHeadline { font-size: 24px; line-height: 32px; }
		.blockSubHeadline { font-size: 22px; line-height: 32px }
	}
	</style>
</head>

<body>

	<?php include("includes/topo.php"); ?>

	<?php include("includes/slider.php"); ?>

	<div class="clear">&nbsp;</div>



	<div id="conteudo">

		<?php include("includes/chamada_servicos.php"); ?>

		<div class="content_coluna">
			<div class="coluna">
				<h4 class="titulo">
					<span class="icon_disklimpeza">&nbsp;</span>
					PRODUTOS
				</h4>

				<ul class="lista">
					<?php
					$result = $obj_site->select("tb_produtos","ORDER BY RAND() LIMIT 3");
					if(mysql_num_rows($result) > 0)
					{
						while($row = mysql_fetch_array($result))
						{
							?>
							<li>
								<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
									<div class="content_descricao">
										<h6><?php Util::imprime($row[titulo]); ?></h6>
										<?php /*<p><?php Util::imprime($row[descricao]); ?></p>*/ ?>
									</div>
								</a>
							</li>
							<?php
						}
					}
					?>
				</ul>

				<a href="<?php echo Util::caminho_projeto(); ?>/produtos/" class="saiba_mais">VER TODAS</a>

			</div>

			<div class="coluna">
				<h4 class="titulo">
					<span class="icon_dicas">&nbsp;</span>
					PARCEIROS
				</h4>

				<ul class="lista1">
					<?php
					$result = $obj_site->select("tb_parceiros","LIMIT 12");
					if(mysql_num_rows($result) > 0)
					{
						while($row = mysql_fetch_array($result))
						{
							?>
							<li>
								<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />


							</li>
							<?php
						}
					}
					?>
				</ul>
				<br><br>
				<a href="<?php echo Util::caminho_projeto(); ?>/parceiros/" class="saiba_mais">VER TODAS</a>

			</div>

			<div class="coluna">
				<h4 class="titulo ">
					<span class="icon_empresa">&nbsp;</span>
					A EMPRESA
				</h4>

				<div class="chamada_empresa">
					<?php
					$result = $obj_site->select("tb_empresa","AND idempresa = 1");
					$row = mysql_fetch_array($result);
					?>
					<a href="" title="Empresa">
						<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="Empresa" />
						<p><?php Util::imprime($row[descricao],346); ?></p>
					</a>
				</div>

				<a href="<?php echo Util::caminho_projeto(); ?>/empresa/" class="saiba_mais">VER MAIS</a>

			</div>
		</div>
	</div>





	<?php include("includes/rodape.php"); ?>

</body>
</html>


<!--	====================================================================================================	 -->
<!--	draggable	 -->
<!--	====================================================================================================	 -->
<link rel="stylesheet" href="././jquery/jquery-ui/jquery-ui.min.css">
<script src="././jquery/jquery-ui/jquery-ui.min.js"></script>


<script>

$( function() {
	$( "#draggable" ).draggable(
		{ containment: "#content_slider", scroll: false });

		$('.fa-times-circle').click(function(e){
			$('#draggable').css('visibility','hidden');
		});


	} );

	</script>
