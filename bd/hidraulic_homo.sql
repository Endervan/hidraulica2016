-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 179.188.16.170
-- Generation Time: 14-Set-2016 às 15:28
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.23-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hidraulic_homo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`, `descricao`) VALUES
(1, 'USINAGEM EM GERAL', '0909201605242722731319.png', 'SIM', 2, '1', 'usinagem-em-geral', '/produtos', NULL, NULL, '<ul>\r\n	<li>\r\n		<span style="color: rgb(0, 0, 0); font-family: Arial; font-size: 11px; text-align: justify;">Restaura&ccedil;&atilde;o de componentes e pe&ccedil;as</span></li>\r\n	<li>\r\n		<span style="color: rgb(0, 0, 0); font-family: Arial; font-size: 11px; text-align: justify;">Conforme Especifica&ccedil;&otilde;es do fabricante</span></li>\r\n	<li>\r\n		<span style="color: rgb(0, 0, 0); font-family: Arial; font-size: 11px; text-align: justify;">Alto padr&atilde;o de qualidade</span></li>\r\n</ul>'),
(3, 'PEÇAS', '0909201606359817247597.png', 'SIM', 4, '1', 'pecas', '/produtos/categoria/bandeiras-diversas', NULL, NULL, '<ul>\r\n	<li>\r\n		Banners e adesivos</li>\r\n	<li>\r\n		Faixas de ilha</li>\r\n	<li>\r\n		Faixas em lona</li>\r\n</ul>'),
(4, 'RETÍFICA DE PEÇAS', '0909201604209312814536.png', 'SIM', 3, '1', 'retifica-de-pecas', '/produtos/', NULL, NULL, '<ul>\r\n	<li>\r\n		Acabamento de pe&ccedil;as cil&iacute;ndricas</li>\r\n	<li>\r\n		Perfeita restaura&ccedil;&atilde;o&nbsp;</li>\r\n	<li>\r\n		M&aacute;quinas e equipe especializadas</li>\r\n</ul>'),
(5, 'Mobile - USINAGEM', '1009201609318908496581.jpg', 'SIM', NULL, '2', 'mobile--usinagem', '', NULL, NULL, ''),
(6, 'Mobile - RETIFICA', '1009201609165817446153.jpg', 'SIM', NULL, '2', 'mobile--retifica', '', NULL, NULL, ''),
(7, 'Mobile - SERVICOS REALIZADOS', '1009201609564563814749.jpg', 'SIM', NULL, '2', 'mobile--servicos-realizados', '', NULL, NULL, ''),
(10, 'SERVIÇOS ESPECIALIZADOS', '0909201605553017006388.png', 'SIM', 1, '1', 'servicos-especializados', '/produtos', NULL, NULL, '<ul>\r\n	<li>\r\n		CROMAGEM</li>\r\n	<li>\r\n		USINAGEM</li>\r\n	<li>\r\n		RET&Iacute;FICA</li>\r\n	<li>\r\n		BRUNIMENTO</li>\r\n</ul>'),
(11, 'Mobile Empresa', '3008201609131389163787.jpg', 'SIM', NULL, '2', 'mobile-empresa', '', NULL, NULL, ''),
(12, 'Mobile Serviços', '3008201609371348409733.jpg', 'SIM', NULL, '2', 'mobile-servicos', '', NULL, NULL, ''),
(13, 'Mobile Serviços Dentro', '3008201609591361155069.jpg', 'SIM', NULL, '2', 'mobile-servicos-dentro', '', NULL, NULL, ''),
(14, 'Mobile Banner Parceiros', '3008201611071332507715.jpg', 'SIM', NULL, '2', 'mobile-banner-parceiros', '', NULL, NULL, ''),
(15, 'Mobile Banner Contatos', '3008201611531350873041.jpg', 'SIM', NULL, '2', 'mobile-banner-contatos', '', NULL, NULL, ''),
(16, 'Mobile Trabalhe Conosco', '3008201612041120024634.jpg', 'SIM', NULL, '2', 'mobile-trabalhe-conosco', '', NULL, NULL, ''),
(17, 'Mobile Orçamentos', '3008201612121394856765.jpg', 'SIM', NULL, '2', 'mobile-orcamentos', '', NULL, NULL, ''),
(18, 'Mobile Banner Produtos Categoria', '3008201612521124294367.jpg', 'SIM', NULL, '2', 'mobile-banner-produtos-categoria', '', NULL, NULL, ''),
(19, 'Mobile Produtos', '3008201601031214843726.jpg', 'SIM', NULL, '', 'mobile-produtos', '', NULL, NULL, ''),
(20, 'Mobile produtos Dentro', '3008201601031347834961.jpg', 'SIM', NULL, '', 'mobile-produtos-dentro', '', NULL, NULL, ''),
(21, 'Mobile - SERVICOS REALIZADOS 2', '1009201609556423634766.jpg', 'SIM', NULL, '2', 'mobile--servicos-realizados-2', '', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(45) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `imagem`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`) VALUES
(1, 'HIDRAULICAS', '0809201604037200534380.jpg', 'SIM', 2, '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid maxime laudantium ducimus voluptatum delectus repellat obcaecati, enim temporibus totam sunt cupiditate quam deserunt, aut accusamus, molestias similique earum ullam. Illo.', '', 'hidraulicas'),
(2, 'Flâmulas e Estandartes', '2308201607381400175684.jpg', 'SIM', 3, '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid maxime laudantium ducimus voluptatum delectus repellat obcaecati, enim temporibus totam sunt cupiditate quam deserunt, aut accusamus, molestias similique earum ullam. Illo.', '', 'flamulas-e-estandartes'),
(3, 'PEÇAS', '0909201603103518856573.jpg', 'SIM', 4, '', '', '', 'pecas'),
(8, 'BANDEIRAS POLITICAS', '2308201607381400175684.jpg', 'SIM', 1, '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid maxime laudantium ducimus voluptatum delectus repellat obcaecati, enim temporibus totam sunt cupiditate quam deserunt, aut accusamus, molestias similique earum ullam. Illo.', '', 'bandeiras-politicas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categoria_servicos`
--

CREATE TABLE `tb_categoria_servicos` (
  `idcategoriaservico` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(45) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_categoria_servicos`
--

INSERT INTO `tb_categoria_servicos` (`idcategoriaservico`, `titulo`, `imagem`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`) VALUES
(3, 'USINAGEM', '0909201603231504641452.jpg', 'SIM', 4, '', '', '', 'usinagem'),
(5, 'CROMAGEM', '0909201603143037986047.jpg', 'SIM', 6, '', '', '', 'cromagem'),
(7, 'BRUNIMENTO', '0909201603153539820925.jpg', 'SIM', 5, '', '', '', 'brunimento'),
(9, 'RETÍFICA', '0909201603194294634614.jpg', 'SIM', 0, '', '', '', 'retifica'),
(10, 'LAPIDAÇÃO', '0909201603257867002988.jpg', 'SIM', 0, '', '', '', 'lapidacao');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `title_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco1` varchar(255) NOT NULL,
  `endereco2` varchar(255) NOT NULL,
  `endereco3` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `telefone3` varchar(255) NOT NULL,
  `estado1` text NOT NULL,
  `estado2` text NOT NULL,
  `estado3` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `src_place` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco1`, `endereco2`, `endereco3`, `telefone1`, `telefone2`, `telefone3`, `estado1`, `estado2`, `estado3`, `email`, `latitude`, `longitude`, `src_place`, `google_plus`) VALUES
(1, '', '', '', 'SIM', 0, '', 'Avenida Castelo Branco, nº. 3738 Setor Rodoviário - Goiânia - GO.', 'Sia/Sul, Qd.05-C, Lt.25/6,Loja 32,  SIA - Brasília - DF.', '', '(62) 3576-9600', '(61) 3234-7523', '(69) 3229-0270', 'GO', 'DF', 'RO', 'junior@homewebbrasil.com.br', '-16.6848918', '-49.2135197', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3822.089308822047!2d-49.30410188541537!3d-16.672413749602143!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef43d8ae16d3b%3A0x60afb7d7a52ace9f!2sHidr%C3%A1ulica+Brasil!5e0!3m2!1spt-BR!2sbr!4v14', 'https://plus.google.com/107323488773929750963');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL DEFAULT 'dica.jpg',
  `descricao` longtext NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `ativo`, `ordem`, `data`) VALUES
(1, 'dicas 1', '2308201607451173414544.jpg', '<div>\r\n	dica 01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', '', '', '', 'dicas-1', 'SIM', 0, '0000-00-00'),
(2, 'dicas 2', '2308201607451173414544.jpg', '<div>\r\n	dica 01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', '', '', '', 'dicas-2', 'SIM', 0, '0000-00-00'),
(3, 'dica teste final com multiplas linhas', '2308201607471264985719.jpg', '', '', '', '', 'dica-teste-final-com-multiplas-linhas', 'SIM', 0, '2016-08-04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` longtext NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(45) NOT NULL,
  `tumb_imagem` varchar(45) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `imagem`, `tumb_imagem`, `url_amigavel`) VALUES
(1, 'Empresa Home', '<p>\r\n	UMA HIST&Oacute;RIA DE SUCESSO!</p>\r\n<p>\r\n	Em 1984, surgia a Hidr&aacute;ulica Brasil, fruto do trabalho pioneiro de Paulo Vieira de Carvalho, fundador apaixonado pela empresa e idealizador de in&uacute;meros avan&ccedil;os tecnol&oacute;gicos no ramo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A HB, com trabalho duro e dedica&ccedil;&atilde;o, conquistou a posi&ccedil;&atilde;o de lideran&ccedil;a de mercado que ostenta atualmente, e &eacute; uma das mais importantes e respeitadas em sua &aacute;rea.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A principal filosofia da Hidr&aacute;ulica Brasil &eacute; atender por completo a necessidade do cliente, e com esse intuito, ela investe constantemente em novas tecnologias e conta com uma estrutura completa para a execu&ccedil;&atilde;o de diversos servi&ccedil;os.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	RESPONSABILIDADE SOCIAL</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Hidr&aacute;ulica Brasil se preocupa com o mundo &agrave; sua volta, e procura contribuir de maneira pro ativa, encampando a no&ccedil;&atilde;o de co-responsabilidade da empresa pelos problemas sociais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Desde 1996 que a HB incentiva e ap&oacute;ia a pr&aacute;tica de esportes, pois acredita na import&acirc;ncia do mesmo no desenvolvimento s&oacute;cio-educacional do cidad&atilde;o e na melhoria da qualidade de vida.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;A integra&ccedil;&atilde;o na pr&aacute;tica esportiva promove inclus&atilde;o social e a troca de experi&ecirc;ncias, despertando valores &eacute;ticos e morais para uma vida em sociedade. Sendo assim a &nbsp;HB caracteriza-se como uma empresa cidad&atilde;, que n&atilde;o foge aos compromissos de trabalhar para a melhoria da qualidade de vida de toda a sociedade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Associa&ccedil;&atilde;o Atl&eacute;tica Hidr&aacute;ulica BrasilASSOCIA&Ccedil;&Atilde;O ATL&Eacute;TICA HIDR&Aacute;ULICA BRASIL</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Embu&iacute;da deste esp&iacute;rito de empresa cidad&atilde;, em 2005, a Hidr&aacute;ulica Brasil fundou a Associa&ccedil;&atilde;o Atl&eacute;tica Hidr&aacute;ulica Brasil -AAHB , com o objetivo de &nbsp;apoiar &nbsp;a inicia&ccedil;&atilde;o e o desenvolvimento esportivo, pois acredita que, ao fazerem parte de programas esportivos, os atletas se distanciam de diversos riscos sociais, al&eacute;m de proporcionar lazer e integra&ccedil;&atilde;o com os colaboradores da empresa durante eventos competitivos dos quais participam.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Atualmente, a AAHB se destaca na modalidade Futsal Feminino, em tr&ecirc;s categorias: Sub-17, Sub-20 e Adulto (acima de 16 anos), envolvendo cerca de 50 atletas de 13 a 30 anos de idade, e tamb&eacute;m faz parcerias com pesquisadores das &aacute;reas de fisioterapia e educa&ccedil;&atilde;o f&iacute;sica.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Al&eacute;m do treinamento e da participa&ccedil;&atilde;o em diversos campeonatos regionais e nacionais, a AAHB estimula suas atletas por meio de palestras educativas, despertando a auto-estima, incentiva e auxilia a continuarem sua forma&ccedil;&atilde;o escolar e a engajarem no meio profissional.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O futsal feminino da Hidr&aacute;ulica Brasil &eacute; atualmente a melhor equipe da categoria na regi&atilde;o Centro-Oeste e est&aacute; entre as quatro melhores do pa&iacute;s. As atletas envolvidas s&atilde;o reconhecidas e respeitadas nacionalmente e seu trabalho serve de modelo para a contribui&ccedil;&atilde;o que o esporte pode oferecer para a sociedade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	RESPONSABILIDADE ECOL&Oacute;GICA</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Todo trabalho tem algum resultado, e no caso da Hidr&aacute;ulica Brasil, al&eacute;m da qualidade de seus servi&ccedil;os, ela tamb&eacute;m tem consci&ecirc;ncia dos res&iacute;duos que s&atilde;o gerados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Efluentes, res&iacute;duos s&oacute;lidos e emiss&otilde;es atmosf&eacute;ricas s&atilde;o controlados de perto pela HB por meio de seu Plano de Controle Ambiental &ndash; PCA, que &eacute; constitu&iacute;do pelos: Projeto de Tratamento de Efluentes, Projeto de Tratamento de Polui&ccedil;&atilde;o Atmosf&eacute;rica e Plano de Gerenciamento de Res&iacute;duos S&oacute;lidos, todos aprovados pelos &oacute;rg&atilde;os ambientais competentes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A HB tem profundo interesse na manuten&ccedil;&atilde;o do meio ambiente e no cumprimento da legisla&ccedil;&atilde;o ambiental, e por isso, nos adaptamos para tomar todas as medidas necess&aacute;rias para o correto gerenciamento de nossos res&iacute;duos.</p>', '', '', '', 'SIM', 0, '0809201603479353029934.jpg', 'tumb_1005201312061164920694.jpg', 'empresa-home'),
(2, 'Parceiros', '', '', '', '', 'SIM', 0, '', '', 'parceiros'),
(3, 'Empresa', '<p>\r\n	UMA HIST&Oacute;RIA DE SUCESSO!</p>\r\n<p>\r\n	Em 1984, surgia a Hidr&aacute;ulica Brasil, fruto do trabalho pioneiro de Paulo Vieira de Carvalho, fundador apaixonado pela empresa e idealizador de in&uacute;meros avan&ccedil;os tecnol&oacute;gicos no ramo.</p>\r\n<p>\r\n	A HB, com trabalho duro e dedica&ccedil;&atilde;o, conquistou a posi&ccedil;&atilde;o de lideran&ccedil;a de mercado que ostenta atualmente, e &eacute; uma das mais importantes e respeitadas em sua &aacute;rea.</p>\r\n<p>\r\n	A principal filosofia da Hidr&aacute;ulica Brasil &eacute; atender por completo a necessidade do cliente, e com esse intuito, ela investe constantemente em novas tecnologias e conta com uma estrutura completa para a execu&ccedil;&atilde;o de diversos servi&ccedil;os.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	RESPONSABILIDADE SOCIAL</p>\r\n<p>\r\n	A Hidr&aacute;ulica Brasil se preocupa com o mundo &agrave; sua volta, e procura contribuir de maneira pro ativa, encampando a no&ccedil;&atilde;o de co-responsabilidade da empresa pelos problemas sociais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Desde 1996 que a HB incentiva e ap&oacute;ia a pr&aacute;tica de esportes, pois acredita na import&acirc;ncia do mesmo no desenvolvimento s&oacute;cio-educacional do cidad&atilde;o e na melhoria da qualidade de vida.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;A integra&ccedil;&atilde;o na pr&aacute;tica esportiva promove inclus&atilde;o social e a troca de experi&ecirc;ncias, despertando valores &eacute;ticos e morais para uma vida em sociedade. Sendo assim a &nbsp;HB caracteriza-se como uma empresa cidad&atilde;, que n&atilde;o foge aos compromissos de trabalhar para a melhoria da qualidade de vida de toda a sociedade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Associa&ccedil;&atilde;o Atl&eacute;tica Hidr&aacute;ulica BrasilASSOCIA&Ccedil;&Atilde;O ATL&Eacute;TICA HIDR&Aacute;ULICA BRASIL</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Embu&iacute;da deste esp&iacute;rito de empresa cidad&atilde;, em 2005, a Hidr&aacute;ulica Brasil fundou a Associa&ccedil;&atilde;o Atl&eacute;tica Hidr&aacute;ulica Brasil -AAHB , com o objetivo de &nbsp;apoiar &nbsp;a inicia&ccedil;&atilde;o e o desenvolvimento esportivo, pois acredita que, ao fazerem parte de programas esportivos, os atletas se distanciam de diversos riscos sociais, al&eacute;m de proporcionar lazer e integra&ccedil;&atilde;o com os colaboradores da empresa durante eventos competitivos dos quais participam.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Atualmente, a AAHB se destaca na modalidade Futsal Feminino, em tr&ecirc;s categorias: Sub-17, Sub-20 e Adulto (acima de 16 anos), envolvendo cerca de 50 atletas de 13 a 30 anos de idade, e tamb&eacute;m faz parcerias com pesquisadores das &aacute;reas de fisioterapia e educa&ccedil;&atilde;o f&iacute;sica.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Al&eacute;m do treinamento e da participa&ccedil;&atilde;o em diversos campeonatos regionais e nacionais, a AAHB estimula suas atletas por meio de palestras educativas, despertando a auto-estima, incentiva e auxilia a continuarem sua forma&ccedil;&atilde;o escolar e a engajarem no meio profissional.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O futsal feminino da Hidr&aacute;ulica Brasil &eacute; atualmente a melhor equipe da categoria na regi&atilde;o Centro-Oeste e est&aacute; entre as quatro melhores do pa&iacute;s. As atletas envolvidas s&atilde;o reconhecidas e respeitadas nacionalmente e seu trabalho serve de modelo para a contribui&ccedil;&atilde;o que o esporte pode oferecer para a sociedade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	RESPONSABILIDADE ECOL&Oacute;GICA</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Todo trabalho tem algum resultado, e no caso da Hidr&aacute;ulica Brasil, al&eacute;m da qualidade de seus servi&ccedil;os, ela tamb&eacute;m tem consci&ecirc;ncia dos res&iacute;duos que s&atilde;o gerados.&nbsp;</p>\r\n<p>\r\n	Efluentes, res&iacute;duos s&oacute;lidos e emiss&otilde;es atmosf&eacute;ricas s&atilde;o controlados de perto pela HB por meio de seu Plano de Controle Ambiental &ndash; PCA, que &eacute; constitu&iacute;do pelos: Projeto de Tratamento de Efluentes, Projeto de Tratamento de Polui&ccedil;&atilde;o Atmosf&eacute;rica e Plano de Gerenciamento de Res&iacute;duos S&oacute;lidos, todos aprovados pelos &oacute;rg&atilde;os ambientais competentes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A HB tem profundo interesse na manuten&ccedil;&atilde;o do meio ambiente e no cumprimento da legisla&ccedil;&atilde;o ambiental, e por isso, nos adaptamos para tomar todas as medidas necess&aacute;rias para o correto gerenciamento de nossos res&iacute;duos.</p>', '', '', '', 'SIM', 0, '0809201603514595606476.jpg', '', 'empresa'),
(4, 'Serviços', '<p>\r\n	servicos Teste orem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '', '', 'SIM', 0, '', '', 'servicos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `idgaleriaproduto` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `id_produto` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`idgaleriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(206, '0109201606286447506435.jpg', 'SIM', 0, '', 219),
(208, '1109201601189690870108.jpg', 'SIM', 0, '', 219);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_servicos`
--

CREATE TABLE `tb_galerias_servicos` (
  `idgaleriaservico` int(11) NOT NULL,
  `imagem` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE latin1_general_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `tb_galerias_servicos`
--

INSERT INTO `tb_galerias_servicos` (`idgaleriaservico`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_servico`) VALUES
(3, '1304201606295203220206.jpg', 'SIM', NULL, NULL, 16),
(4, '1304201606446323607750.jpg', 'SIM', NULL, NULL, 16),
(6, '1304201606536811737716.jpg', 'SIM', NULL, NULL, 16),
(8, '1304201607083048198525.jpg', 'SIM', NULL, NULL, 16),
(9, '1404201601286487392650.jpg', 'SIM', NULL, NULL, 16),
(10, '1404201601295481475223.jpg', 'SIM', NULL, NULL, 16),
(12, '0909201603139434194726.jpg', 'SIM', NULL, NULL, 219),
(13, '0909201603165865759990.jpg', 'SIM', NULL, NULL, 220),
(14, '0909201603194431820350.jpg', 'SIM', NULL, NULL, 240),
(15, '0909201603227740607033.jpg', 'SIM', NULL, NULL, 221),
(16, '0909201603262633761084.jpg', 'SIM', NULL, NULL, 241),
(17, '1009201610004458128313.jpg', 'SIM', NULL, NULL, 219),
(18, '1109201612583545998545.jpg', 'SIM', NULL, NULL, 241);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_grupos_logins`
--

CREATE TABLE `tb_grupos_logins` (
  `idgrupologin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `imagem` varchar(45) DEFAULT NULL,
  `exibir_menu` varchar(3) DEFAULT 'SIM',
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_grupos_logins`
--

INSERT INTO `tb_grupos_logins` (`idgrupologin`, `nome`, `imagem`, `exibir_menu`, `ativo`, `url_amigavel`) VALUES
(1, 'Administradores', NULL, 'SIM', 'SIM', ''),
(9, 'Contabilidade', NULL, 'SIM', 'SIM', ''),
(10, 'Cliente', NULL, 'SIM', 'SIM', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_grupos_logins_tb_paginas`
--

CREATE TABLE `tb_grupos_logins_tb_paginas` (
  `id_grupologin` int(11) NOT NULL,
  `id_pagina` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_grupos_logins_tb_paginas`
--

INSERT INTO `tb_grupos_logins_tb_paginas` (`id_grupologin`, `id_pagina`, `url_amigavel`) VALUES
(1, 148, ''),
(1, 149, ''),
(1, 150, ''),
(1, 151, ''),
(1, 152, ''),
(1, 153, ''),
(1, 154, ''),
(1, 155, ''),
(1, 156, ''),
(1, 157, ''),
(1, 158, ''),
(1, 159, ''),
(1, 160, ''),
(1, 161, ''),
(1, 162, ''),
(1, 163, ''),
(1, 164, ''),
(1, 165, ''),
(1, 166, ''),
(1, 167, ''),
(1, 168, ''),
(1, 169, ''),
(1, 170, ''),
(1, 171, ''),
(1, 172, ''),
(1, 173, ''),
(1, 174, ''),
(1, 175, ''),
(1, 176, ''),
(1, 177, ''),
(1, 178, ''),
(1, 179, ''),
(1, 180, ''),
(1, 181, ''),
(1, 182, ''),
(1, 183, ''),
(1, 184, ''),
(1, 185, ''),
(1, 186, ''),
(1, 187, ''),
(1, 188, ''),
(1, 189, ''),
(1, 190, ''),
(1, 191, ''),
(1, 192, ''),
(1, 193, ''),
(1, 194, ''),
(1, 195, ''),
(1, 196, ''),
(1, 197, ''),
(1, 198, ''),
(1, 199, ''),
(1, 200, ''),
(1, 201, ''),
(1, 202, ''),
(10, 159, ''),
(10, 160, ''),
(10, 162, ''),
(10, 163, ''),
(10, 164, ''),
(10, 165, ''),
(10, 166, ''),
(10, 167, ''),
(10, 168, ''),
(10, 169, ''),
(10, 170, ''),
(10, 171, ''),
(10, 172, ''),
(10, 173, ''),
(10, 174, ''),
(10, 175, ''),
(10, 176, ''),
(10, 177, ''),
(10, 178, ''),
(10, 179, ''),
(10, 180, ''),
(10, 181, ''),
(10, 182, ''),
(10, 183, ''),
(10, 184, ''),
(10, 185, ''),
(10, 186, ''),
(10, 187, ''),
(10, 188, ''),
(10, 191, ''),
(10, 192, ''),
(10, 193, ''),
(10, 194, ''),
(10, 195, ''),
(10, 196, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`, `url_amigavel`) VALUES
(1, 'Marcio André da Silva', 'ab3e61b12b184adf6aa56b2b708ca92d', 'SIM', 1, 'marciomas@gmail.com', ''),
(3, 'junior', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 1, 'atendimento.sites@homewebbrasil.com.br', ''),
(5, 'contato.sobandeiras@gmail.com', 'bdb8d2441c4a5fe26ff38daf81ac1101', 'SIM', 10, 'contato.sobandeiras@gmail.com', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`, `url_amigavel`) VALUES
(705, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '01:02:38', 2, ''),
(706, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '01:04:09', 2, ''),
(707, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '12:54:31', 2, ''),
(708, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '12:58:02', 2, ''),
(709, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '12:59:30', 2, ''),
(710, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '13:26:36', 2, ''),
(711, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '13:29:41', 2, ''),
(712, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '13:31:16', 2, ''),
(713, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '13:33:13', 2, ''),
(714, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '14:11:46', 2, ''),
(715, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '14:13:33', 2, ''),
(716, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '14:18:12', 2, ''),
(717, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '14:25:07', 2, ''),
(718, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '14:29:06', 2, ''),
(719, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '14:31:55', 2, ''),
(720, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '14:55:23', 2, ''),
(721, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '15:00:06', 2, ''),
(722, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '15:00:37', 2, ''),
(723, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '15:01:23', 2, ''),
(724, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '15:03:20', 2, ''),
(725, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '15:18:58', 2, ''),
(726, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '15:19:31', 2, ''),
(727, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '15:27:45', 2, ''),
(728, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:34:20', 2, ''),
(729, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:37:34', 2, ''),
(730, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:38:53', 2, ''),
(731, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:43:08', 2, ''),
(732, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:43:28', 2, ''),
(733, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:43:42', 2, ''),
(734, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:48:59', 2, ''),
(735, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:49:10', 2, ''),
(736, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:49:21', 2, ''),
(737, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:49:34', 2, ''),
(738, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:49:45', 2, ''),
(739, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:50:06', 2, ''),
(740, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:51:17', 2, ''),
(741, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:51:55', 2, ''),
(742, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:52:08', 2, ''),
(743, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:52:23', 2, ''),
(744, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '15:52:36', 2, ''),
(745, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '18:00:04', 2, ''),
(746, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '18:01:01', 2, ''),
(747, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '18:02:34', 2, ''),
(748, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '19:36:39', 2, ''),
(749, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '19:37:31', 2, ''),
(750, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '19:38:27', 2, ''),
(751, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '19:40:21', 2, ''),
(752, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '19:42:33', 2, ''),
(753, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '19:43:01', 2, ''),
(754, 'CADASTRO DO CLIENTE ', '', '2013-05-04', '19:45:10', 2, ''),
(755, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '19:45:51', 2, ''),
(756, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-04', '19:57:54', 2, ''),
(757, 'CADASTRO DO CLIENTE ', '', '2013-05-07', '23:53:34', 2, ''),
(758, 'CADASTRO DO CLIENTE ', '', '2013-05-08', '00:08:44', 2, ''),
(759, 'CADASTRO DO CLIENTE ', '', '2013-05-08', '00:09:58', 2, ''),
(760, 'CADASTRO DO CLIENTE ', '', '2013-05-09', '23:25:41', 2, ''),
(761, 'CADASTRO DO CLIENTE ', '', '2013-05-09', '23:27:17', 2, ''),
(762, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:27:58', 2, ''),
(763, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:29:17', 2, ''),
(764, 'CADASTRO DO CLIENTE ', '', '2013-05-09', '23:31:17', 2, ''),
(765, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2013-05-09', '23:33:02', 2, ''),
(766, 'CADASTRO DO CLIENTE ', '', '2013-05-09', '23:34:36', 2, ''),
(767, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:36:52', 2, ''),
(768, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:37:40', 2, ''),
(769, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:38:38', 2, ''),
(770, 'CADASTRO DO CLIENTE ', '', '2013-05-09', '23:39:43', 2, ''),
(771, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:40:24', 2, ''),
(772, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:42:42', 2, ''),
(773, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:44:22', 2, ''),
(774, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-09', '23:46:22', 2, ''),
(775, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:06:03', 2, ''),
(776, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:13:22', 2, ''),
(777, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:13:43', 2, ''),
(778, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:14:03', 2, ''),
(779, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:14:22', 2, ''),
(780, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:14:41', 2, ''),
(781, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:21:49', 2, ''),
(782, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:23:30', 2, ''),
(783, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '00:24:21', 2, ''),
(784, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '23:11:35', 2, ''),
(785, 'CADASTRO DO CLIENTE ', '', '2013-05-10', '23:23:29', 2, ''),
(786, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-10', '23:39:59', 2, ''),
(787, 'CADASTRO DO CLIENTE ', '', '2013-05-10', '23:40:52', 2, ''),
(788, 'CADASTRO DO CLIENTE ', '', '2013-05-10', '23:43:12', 2, ''),
(789, 'CADASTRO DO CLIENTE ', '', '2013-05-10', '23:44:33', 2, ''),
(790, 'CADASTRO DO CLIENTE ', '', '2013-05-10', '23:45:25', 2, ''),
(791, 'CADASTRO DO CLIENTE ', '', '2013-05-10', '23:46:51', 2, ''),
(792, 'CADASTRO DO CLIENTE ', '', '2013-05-10', '23:47:01', 2, ''),
(793, 'CADASTRO DO CLIENTE ', '', '2013-05-10', '23:47:13', 2, ''),
(794, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '00:57:50', 2, ''),
(795, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '01:00:27', 2, ''),
(796, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '01:01:30', 2, ''),
(797, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '01:07:32', 2, ''),
(798, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '01:08:35', 2, ''),
(799, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '01:12:48', 2, ''),
(800, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '01:14:28', 2, ''),
(801, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '01:14:48', 2, ''),
(802, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '13:40:22', 2, ''),
(803, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '13:42:02', 2, ''),
(804, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '15:21:05', 2, ''),
(805, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '15:24:27', 2, ''),
(806, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '15:24:47', 2, ''),
(807, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '15:25:22', 2, ''),
(808, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '15:52:22', 2, ''),
(809, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:08:54', 2, ''),
(810, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:09:11', 2, ''),
(811, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:10:11', 2, ''),
(812, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:12:20', 2, ''),
(813, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:16:47', 2, ''),
(814, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:16:57', 2, ''),
(815, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:21:47', 2, ''),
(816, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:21:59', 2, ''),
(817, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:24:24', 2, ''),
(818, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:24:54', 2, ''),
(819, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:31:19', 2, ''),
(820, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '18:32:39', 2, ''),
(821, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:33:17', 2, ''),
(822, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'7\'', '2013-05-11', '18:33:41', 2, ''),
(823, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'7\'', '2013-05-11', '18:34:26', 2, ''),
(824, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'6\'', '2013-05-11', '18:34:41', 2, ''),
(825, 'CADASTRO DO CLIENTE ', '', '2013-05-11', '18:36:32', 2, ''),
(826, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:37:24', 2, ''),
(827, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:38:09', 2, ''),
(828, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '18:38:25', 2, ''),
(829, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '16:44:45', 3, ''),
(830, 'ALTERAÇÃO DO CLIENTE ', '', '2013-05-11', '16:46:57', 3, ''),
(831, 'CADASTRO DO LOGIN Danilo', 'INSERT INTO	tb_logins\n					(nome, senha, email, id_grupologin)\n					VALUES\n					(\'Danilo\', \'a244617c9b2ededab077000718b1ff76\', \'vendas@disklimp.com\', \'1\')', '2013-07-19', '08:19:46', 3, ''),
(832, 'ALTERAÇÃO DO CLIENTE ', '', '2013-07-24', '17:28:38', 1, ''),
(833, 'ALTERAÇÃO DO CLIENTE ', '', '2013-08-05', '12:52:33', 3, ''),
(834, 'ALTERAÇÃO DO CLIENTE ', '', '2013-08-05', '13:47:11', 3, ''),
(835, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'1\'', '2013-08-05', '14:05:59', 3, ''),
(836, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'2\'', '2013-08-05', '14:06:26', 3, ''),
(837, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'3\'', '2013-08-05', '14:06:34', 3, ''),
(838, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2013-08-05', '14:07:23', 3, ''),
(839, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2013-08-05', '14:07:31', 3, ''),
(840, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2013-08-05', '14:07:38', 3, ''),
(841, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2013-08-05', '14:07:45', 3, ''),
(842, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_marcas_produtos WHERE idmarcaproduto = \'1\'', '2013-08-05', '14:08:07', 3, ''),
(843, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_marcas_produtos WHERE idmarcaproduto = \'2\'', '2013-08-05', '14:08:14', 3, ''),
(844, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_marcas_produtos WHERE idmarcaproduto = \'3\'', '2013-08-05', '14:08:20', 3, ''),
(845, 'ALTERAÇÃO DO CLIENTE ', '', '2013-08-05', '14:58:23', 3, ''),
(846, 'CADASTRO DO CLIENTE ', '', '2013-08-05', '16:03:24', 3, ''),
(847, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-17', '23:36:42', 3, ''),
(848, 'CADASTRO DO CLIENTE ', '', '2013-09-17', '23:42:25', 3, ''),
(849, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'5\'', '2013-09-17', '23:44:01', 3, ''),
(850, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-17', '23:48:02', 3, ''),
(851, 'CADASTRO DO CLIENTE ', '', '2013-09-17', '23:56:15', 3, ''),
(852, 'CADASTRO DO CLIENTE ', '', '2013-09-17', '23:56:51', 3, ''),
(853, 'CADASTRO DO CLIENTE ', '', '2013-09-17', '23:59:58', 3, ''),
(854, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'12\'', '2013-09-18', '00:10:59', 3, ''),
(855, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'14\'', '2013-09-18', '00:11:05', 3, ''),
(856, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-18', '00:13:02', 3, ''),
(857, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-18', '00:14:27', 3, ''),
(858, 'CADASTRO DO CLIENTE ', '', '2013-09-18', '00:17:11', 3, ''),
(859, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-18', '00:26:02', 3, ''),
(860, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'15\'', '2013-09-19', '19:05:32', 3, ''),
(861, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:07:55', 3, ''),
(862, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:10:41', 3, ''),
(863, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:11:30', 3, ''),
(864, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'17\'', '2013-09-19', '19:12:51', 3, ''),
(865, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:16:10', 3, ''),
(866, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:18:42', 3, ''),
(867, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:21:03', 3, ''),
(868, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:24:15', 3, ''),
(869, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:26:38', 3, ''),
(870, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:29:20', 3, ''),
(871, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:33:39', 3, ''),
(872, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:34:50', 3, ''),
(873, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:37:58', 3, ''),
(874, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:39:27', 3, ''),
(875, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:41:18', 3, ''),
(876, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:43:12', 3, ''),
(877, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:45:02', 3, ''),
(878, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:47:31', 3, ''),
(879, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '19:49:39', 3, ''),
(880, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-19', '19:59:22', 3, ''),
(881, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '20:03:14', 3, ''),
(882, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-19', '20:07:55', 3, ''),
(883, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-19', '20:08:49', 3, ''),
(884, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '20:51:36', 3, ''),
(885, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '20:53:52', 3, ''),
(886, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '20:55:42', 3, ''),
(887, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '20:57:25', 3, ''),
(888, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '20:59:35', 3, ''),
(889, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:02:08', 3, ''),
(890, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:04:40', 3, ''),
(891, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:40:05', 3, ''),
(892, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:42:00', 3, ''),
(893, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:51:35', 3, ''),
(894, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:52:16', 3, ''),
(895, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'10\'', '2013-09-19', '21:54:17', 3, ''),
(896, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:54:51', 3, ''),
(897, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:56:32', 3, ''),
(898, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '21:58:18', 3, ''),
(899, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:00:27', 3, ''),
(900, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:01:57', 3, ''),
(901, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:16:42', 3, ''),
(902, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:18:23', 3, ''),
(903, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:22:32', 3, ''),
(904, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:23:44', 3, ''),
(905, 'EXCLUSÃO DO LOGIN 51, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'51\'', '2013-09-19', '22:24:21', 3, ''),
(906, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:26:07', 3, ''),
(907, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:27:45', 3, ''),
(908, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:29:49', 3, ''),
(909, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:33:51', 3, ''),
(910, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:36:42', 3, ''),
(911, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:38:55', 3, ''),
(912, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:40:50', 3, ''),
(913, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:42:33', 3, ''),
(914, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-19', '22:43:18', 3, ''),
(915, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:44:56', 3, ''),
(916, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:46:37', 3, ''),
(917, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:49:49', 3, ''),
(918, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:51:23', 3, ''),
(919, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-19', '22:52:28', 3, ''),
(920, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-19', '22:52:45', 3, ''),
(921, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:53:45', 3, ''),
(922, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:55:22', 3, ''),
(923, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:57:13', 3, ''),
(924, 'CADASTRO DO CLIENTE ', '', '2013-09-19', '22:59:27', 3, ''),
(925, 'CADASTRO DO CLIENTE ', '', '2013-09-21', '10:04:37', 3, ''),
(926, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '10:07:13', 3, ''),
(927, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '10:08:06', 3, ''),
(928, 'CADASTRO DO CLIENTE ', '', '2013-09-21', '11:15:49', 3, ''),
(929, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '11:21:20', 3, ''),
(930, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '12:11:56', 3, ''),
(931, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '12:15:15', 3, ''),
(932, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '12:24:46', 3, ''),
(933, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '12:26:48', 3, ''),
(934, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '12:34:00', 3, ''),
(935, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '12:35:09', 3, ''),
(936, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-21', '12:35:49', 3, ''),
(937, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:10:35', 3, ''),
(938, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:11:07', 3, ''),
(939, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:11:30', 3, ''),
(940, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:11:45', 3, ''),
(941, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:12:08', 3, ''),
(942, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:12:28', 3, ''),
(943, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:12:51', 3, ''),
(944, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:13:10', 3, ''),
(945, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:13:52', 3, ''),
(946, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:14:16', 3, ''),
(947, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:14:30', 3, ''),
(948, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:14:57', 3, ''),
(949, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:15:20', 3, ''),
(950, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:16:11', 3, ''),
(951, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:16:26', 3, ''),
(952, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:16:41', 3, ''),
(953, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:16:55', 3, ''),
(954, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:17:10', 3, ''),
(955, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:17:22', 3, ''),
(956, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:17:37', 3, ''),
(957, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:17:53', 3, ''),
(958, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:18:13', 3, ''),
(959, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '16:19:27', 3, ''),
(960, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:20:47', 3, ''),
(961, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:21:03', 3, ''),
(962, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:21:19', 3, ''),
(963, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:21:32', 3, ''),
(964, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:21:50', 3, ''),
(965, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:22:09', 3, ''),
(966, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:22:31', 3, ''),
(967, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:22:50', 3, ''),
(968, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:23:06', 3, ''),
(969, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:23:47', 3, ''),
(970, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:24:03', 3, ''),
(971, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:24:42', 3, ''),
(972, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:25:06', 3, ''),
(973, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:25:22', 3, ''),
(974, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:25:39', 3, ''),
(975, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:25:56', 3, ''),
(976, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:26:40', 3, ''),
(977, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:27:08', 3, ''),
(978, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:27:29', 3, ''),
(979, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:28:14', 3, ''),
(980, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:29:08', 3, ''),
(981, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:40:59', 3, ''),
(982, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:43:41', 3, ''),
(983, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:45:49', 3, ''),
(984, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:47:48', 3, ''),
(985, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '16:50:20', 3, ''),
(986, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '17:00:50', 3, ''),
(987, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '17:06:11', 3, ''),
(988, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '17:08:05', 3, ''),
(989, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '17:10:37', 3, ''),
(990, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '17:14:51', 3, ''),
(991, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '17:17:30', 3, ''),
(992, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '17:28:46', 3, ''),
(993, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '17:31:13', 3, ''),
(994, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '17:33:30', 3, ''),
(995, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '17:37:38', 3, ''),
(996, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '17:39:57', 3, ''),
(997, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '17:40:37', 3, ''),
(998, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '17:45:03', 3, ''),
(999, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-23', '17:46:04', 3, ''),
(1000, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '19:20:30', 2, ''),
(1001, 'EXCLUSÃO DO LOGIN 103, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'103\'', '2013-09-23', '19:20:51', 2, ''),
(1002, 'EXCLUSÃO DO LOGIN 109, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'109\'', '2013-09-23', '19:33:08', 2, ''),
(1003, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '19:34:17', 2, ''),
(1004, 'EXCLUSÃO DO LOGIN 111, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'111\'', '2013-09-23', '19:34:55', 2, ''),
(1005, 'EXCLUSÃO DO LOGIN 111, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'111\'', '2013-09-23', '19:37:53', 2, ''),
(1006, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '20:36:41', 3, ''),
(1007, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '20:40:38', 3, ''),
(1008, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '20:43:38', 3, ''),
(1009, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '20:46:37', 3, ''),
(1010, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '20:54:16', 3, ''),
(1011, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:03:12', 3, ''),
(1012, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:04:29', 3, ''),
(1013, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:10:36', 3, ''),
(1014, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:13:15', 3, ''),
(1015, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:15:24', 3, ''),
(1016, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:27:06', 3, ''),
(1017, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:35:35', 3, ''),
(1018, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:37:58', 3, ''),
(1019, 'CADASTRO DO CLIENTE ', '', '2013-09-23', '21:40:23', 3, ''),
(1020, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:29:29', 3, ''),
(1021, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:32:01', 3, ''),
(1022, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:33:59', 3, ''),
(1023, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:34:58', 3, ''),
(1024, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:35:45', 3, ''),
(1025, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:37:04', 3, ''),
(1026, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:38:10', 3, ''),
(1027, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:41:08', 3, ''),
(1028, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:44:26', 3, ''),
(1029, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:47:16', 3, ''),
(1030, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:49:16', 3, ''),
(1031, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:49:59', 3, ''),
(1032, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:52:03', 3, ''),
(1033, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:53:48', 3, ''),
(1034, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:54:40', 3, ''),
(1035, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:55:17', 3, ''),
(1036, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:57:53', 3, ''),
(1037, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '13:59:46', 3, ''),
(1038, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:03:39', 3, ''),
(1039, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:05:05', 3, ''),
(1040, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:06:42', 3, ''),
(1041, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:10:04', 3, ''),
(1042, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:12:09', 3, ''),
(1043, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:13:50', 3, ''),
(1044, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:14:22', 3, ''),
(1045, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:14:49', 3, ''),
(1046, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:15:16', 3, ''),
(1047, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:15:41', 3, ''),
(1048, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:17:43', 3, ''),
(1049, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:18:19', 3, ''),
(1050, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:19:37', 3, ''),
(1051, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:20:55', 3, ''),
(1052, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:22:41', 3, ''),
(1053, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:25:44', 3, ''),
(1054, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:26:02', 3, ''),
(1055, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:26:34', 3, ''),
(1056, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:27:08', 3, ''),
(1057, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:28:23', 3, ''),
(1058, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '14:39:23', 3, ''),
(1059, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '14:41:52', 3, ''),
(1060, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '14:42:56', 3, ''),
(1061, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '14:46:49', 3, ''),
(1062, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '14:47:54', 3, ''),
(1063, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '14:49:20', 3, ''),
(1064, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:51:23', 3, ''),
(1065, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:51:45', 3, ''),
(1066, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:53:29', 3, ''),
(1067, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '14:54:31', 3, ''),
(1068, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:56:36', 3, ''),
(1069, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '14:57:52', 3, ''),
(1070, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '14:58:19', 3, ''),
(1071, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:00:21', 3, ''),
(1072, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:04:06', 3, ''),
(1073, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:05:11', 3, ''),
(1074, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:05:32', 3, ''),
(1075, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:06:59', 3, ''),
(1076, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:08:20', 3, ''),
(1077, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:08:41', 3, ''),
(1078, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:08:59', 3, ''),
(1079, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:09:13', 3, ''),
(1080, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:10:41', 3, ''),
(1081, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:12:37', 3, ''),
(1082, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:37:46', 3, ''),
(1083, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:40:25', 3, ''),
(1084, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:42:06', 3, ''),
(1085, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:42:23', 3, ''),
(1086, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:42:39', 3, ''),
(1087, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '15:44:23', 3, ''),
(1088, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:46:38', 3, ''),
(1089, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:48:23', 3, ''),
(1090, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:49:37', 3, ''),
(1091, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:52:13', 3, ''),
(1092, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:53:27', 3, ''),
(1093, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:54:40', 3, ''),
(1094, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:55:31', 3, ''),
(1095, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:56:35', 3, ''),
(1096, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:57:43', 3, ''),
(1097, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:58:33', 3, ''),
(1098, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:59:05', 3, ''),
(1099, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:59:22', 3, ''),
(1100, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '15:59:37', 3, ''),
(1101, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:00:18', 3, ''),
(1102, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:00:54', 3, ''),
(1103, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:01:14', 3, ''),
(1104, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:01:35', 3, ''),
(1105, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:02:19', 3, ''),
(1106, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:03:04', 3, ''),
(1107, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:03:20', 3, ''),
(1108, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:03:38', 3, ''),
(1109, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '16:03:58', 3, ''),
(1110, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:15:26', 3, ''),
(1111, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:16:27', 3, ''),
(1112, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:16:38', 3, ''),
(1113, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:17:00', 3, ''),
(1114, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:18:57', 3, ''),
(1115, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:19:05', 3, ''),
(1116, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:19:17', 3, ''),
(1117, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:19:25', 3, ''),
(1118, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:19:34', 3, ''),
(1119, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:19:42', 3, ''),
(1120, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:21:09', 3, ''),
(1121, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:24:56', 3, ''),
(1122, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:25:10', 3, ''),
(1123, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:25:18', 3, ''),
(1124, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:25:26', 3, ''),
(1125, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:25:37', 3, ''),
(1126, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:25:47', 3, ''),
(1127, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:25:57', 3, ''),
(1128, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:26:04', 3, ''),
(1129, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:26:12', 3, ''),
(1130, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'8\'', '2013-09-24', '16:28:34', 3, ''),
(1131, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:28:53', 3, ''),
(1132, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:29:50', 3, ''),
(1133, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:33:44', 3, ''),
(1134, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:33:57', 3, ''),
(1135, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:37:10', 3, ''),
(1136, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:37:19', 3, ''),
(1137, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:37:42', 3, ''),
(1138, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:38:14', 3, ''),
(1139, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:38:33', 3, ''),
(1140, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:44:10', 3, ''),
(1141, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:45:52', 3, ''),
(1142, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:48:29', 3, ''),
(1143, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:53:14', 3, ''),
(1144, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:53:22', 3, ''),
(1145, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:53:32', 3, ''),
(1146, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:53:41', 3, ''),
(1147, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:53:48', 3, ''),
(1148, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:54:06', 3, ''),
(1149, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:54:15', 3, ''),
(1150, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:54:24', 3, ''),
(1151, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:54:33', 3, ''),
(1152, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:54:41', 3, ''),
(1153, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:54:55', 3, ''),
(1154, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:55:05', 3, ''),
(1155, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:55:15', 3, ''),
(1156, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:55:28', 3, ''),
(1157, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:55:40', 3, ''),
(1158, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:55:50', 3, ''),
(1159, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:55:59', 3, ''),
(1160, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:56:09', 3, ''),
(1161, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:56:18', 3, ''),
(1162, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:56:28', 3, ''),
(1163, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:56:36', 3, ''),
(1164, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:56:47', 3, ''),
(1165, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:56:55', 3, ''),
(1166, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:57:05', 3, ''),
(1167, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:57:15', 3, ''),
(1168, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:57:42', 3, ''),
(1169, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '16:58:05', 3, ''),
(1170, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:01:11', 3, ''),
(1171, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:12:13', 3, ''),
(1172, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:12:52', 3, ''),
(1173, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:14:10', 3, ''),
(1174, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:15:02', 3, ''),
(1175, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:15:11', 3, ''),
(1176, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:15:26', 3, ''),
(1177, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:15:36', 3, ''),
(1178, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:16:51', 3, ''),
(1179, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:17:20', 3, ''),
(1180, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:17:38', 3, ''),
(1181, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:17:47', 3, ''),
(1182, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:18:17', 3, ''),
(1183, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:19:08', 3, ''),
(1184, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:19:34', 3, ''),
(1185, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:19:47', 3, ''),
(1186, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:21:14', 3, ''),
(1187, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:21:30', 3, ''),
(1188, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:21:54', 3, ''),
(1189, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:22:44', 3, ''),
(1190, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:23:04', 3, ''),
(1191, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:23:15', 3, ''),
(1192, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:25:50', 3, ''),
(1193, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:26:02', 3, ''),
(1194, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:27:00', 3, ''),
(1195, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:27:13', 3, ''),
(1196, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:27:24', 3, ''),
(1197, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:28:24', 3, ''),
(1198, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:28:45', 3, ''),
(1199, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:28:59', 3, ''),
(1200, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:29:10', 3, ''),
(1201, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:29:22', 3, ''),
(1202, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:34:23', 3, ''),
(1203, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:39:16', 3, ''),
(1204, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:41:13', 3, ''),
(1205, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:42:12', 3, ''),
(1206, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:46:12', 3, ''),
(1207, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:47:56', 3, ''),
(1208, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:49:09', 3, ''),
(1209, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:50:34', 3, ''),
(1210, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:52:12', 3, ''),
(1211, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:53:00', 3, ''),
(1212, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:53:22', 3, ''),
(1213, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:54:39', 3, ''),
(1214, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:55:00', 3, ''),
(1215, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:56:38', 3, ''),
(1216, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '17:57:27', 3, ''),
(1217, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '18:00:58', 3, ''),
(1218, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '18:03:45', 3, ''),
(1219, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '18:05:43', 3, ''),
(1220, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '18:06:38', 3, ''),
(1221, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '18:08:38', 3, ''),
(1222, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '18:16:15', 3, ''),
(1223, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:19:26', 3, ''),
(1224, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:21:30', 3, ''),
(1225, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:22:22', 3, ''),
(1226, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:24:32', 3, ''),
(1227, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:29:29', 3, ''),
(1228, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:31:52', 3, ''),
(1229, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:38:11', 3, ''),
(1230, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:41:57', 3, ''),
(1231, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '19:43:48', 3, ''),
(1232, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '20:10:44', 3, ''),
(1233, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '20:19:11', 3, ''),
(1234, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '20:20:25', 3, ''),
(1235, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '20:23:25', 3, ''),
(1236, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '20:25:29', 3, ''),
(1237, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '20:30:36', 3, ''),
(1238, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '20:33:37', 3, ''),
(1239, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '20:34:24', 3, ''),
(1240, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '20:37:06', 3, ''),
(1241, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '20:43:41', 3, ''),
(1242, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '20:43:56', 3, ''),
(1243, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '20:46:22', 3, ''),
(1244, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '20:58:37', 3, ''),
(1245, 'CADASTRO DO CLIENTE ', '', '2013-09-24', '21:07:33', 3, ''),
(1246, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'13\'', '2013-09-24', '21:07:51', 3, ''),
(1247, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '21:08:41', 3, ''),
(1248, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '21:09:31', 3, ''),
(1249, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '21:10:01', 3, ''),
(1250, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '21:12:45', 3, ''),
(1251, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '21:14:12', 3, ''),
(1252, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-24', '21:18:17', 3, ''),
(1253, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '06:15:58', 3, ''),
(1254, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '06:17:04', 3, ''),
(1255, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '06:20:18', 3, ''),
(1256, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '06:24:59', 3, ''),
(1257, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '08:11:51', 1, ''),
(1258, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:12:22', 1, ''),
(1259, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:13:39', 1, ''),
(1260, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:14:03', 1, ''),
(1261, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'1\'', '2013-09-25', '08:42:01', 3, ''),
(1262, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '08:48:45', 3, ''),
(1263, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:50:00', 3, ''),
(1264, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'4\'', '2013-09-25', '08:50:42', 3, ''),
(1265, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '08:52:50', 3, ''),
(1266, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:53:40', 3, ''),
(1267, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:54:04', 3, ''),
(1268, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '08:57:06', 3, ''),
(1269, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:57:52', 3, ''),
(1270, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:59:23', 3, ''),
(1271, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '08:59:58', 3, ''),
(1272, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:01:27', 3, ''),
(1273, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '09:04:13', 3, ''),
(1274, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'12\'', '2013-09-25', '09:05:00', 3, ''),
(1275, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'11\'', '2013-09-25', '09:05:19', 3, ''),
(1276, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'14\'', '2013-09-25', '09:05:58', 3, ''),
(1277, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '09:07:51', 3, ''),
(1278, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:15:34', 1, ''),
(1279, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:19:41', 1, ''),
(1280, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:21:33', 1, ''),
(1281, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:21:50', 1, ''),
(1282, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:24:15', 3, ''),
(1283, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:25:56', 3, ''),
(1284, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:26:34', 3, ''),
(1285, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:34:14', 3, ''),
(1286, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:35:30', 3, ''),
(1287, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:36:54', 3, ''),
(1288, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:38:29', 3, ''),
(1289, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:40:01', 3, ''),
(1290, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:40:37', 3, ''),
(1291, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:41:10', 3, ''),
(1292, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:41:34', 3, ''),
(1293, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:41:54', 3, ''),
(1294, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:42:54', 3, ''),
(1295, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:43:35', 3, ''),
(1296, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:46:20', 3, ''),
(1297, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '09:46:47', 3, ''),
(1298, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:00:46', 0, ''),
(1299, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:01:59', 3, ''),
(1300, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:02:24', 3, ''),
(1301, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:02:42', 3, ''),
(1302, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:02:58', 3, ''),
(1303, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:03:19', 3, ''),
(1304, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:05:26', 3, ''),
(1305, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:07:20', 3, ''),
(1306, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:07:53', 3, ''),
(1307, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'9\'', '2013-09-25', '10:09:16', 3, ''),
(1308, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'15\'', '2013-09-25', '10:09:39', 3, ''),
(1309, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:11:45', 3, ''),
(1310, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:14:51', 3, ''),
(1311, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:17:13', 3, ''),
(1312, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:19:52', 3, ''),
(1313, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:22:19', 3, ''),
(1314, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:23:15', 3, ''),
(1315, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:23:58', 3, ''),
(1316, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:25:03', 3, ''),
(1317, 'CADASTRO DO CLIENTE ', '', '2013-09-25', '10:25:41', 3, ''),
(1318, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:26:44', 3, ''),
(1319, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:27:13', 3, ''),
(1320, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'2\'', '2013-09-25', '10:29:20', 3, ''),
(1321, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:35:08', 3, ''),
(1322, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:36:06', 3, ''),
(1323, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:36:16', 3, ''),
(1324, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:37:08', 3, ''),
(1325, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:37:40', 3, ''),
(1326, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:38:09', 3, ''),
(1327, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:43:25', 3, ''),
(1328, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '10:45:19', 3, ''),
(1329, 'EXCLUSÃO DO LOGIN 148, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'148\'', '2013-09-25', '10:47:53', 3, ''),
(1330, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '11:08:46', 3, ''),
(1331, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '11:09:25', 3, ''),
(1332, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '11:34:34', 3, ''),
(1333, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '11:35:15', 3, ''),
(1334, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:25:00', 3, ''),
(1335, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:25:33', 3, ''),
(1336, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:33:10', 3, ''),
(1337, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:34:26', 3, ''),
(1338, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:35:26', 3, ''),
(1339, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:37:00', 3, ''),
(1340, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:42:08', 3, ''),
(1341, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:45:15', 3, ''),
(1342, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:48:03', 3, ''),
(1343, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:48:24', 3, ''),
(1344, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:48:47', 3, ''),
(1345, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:50:58', 3, ''),
(1346, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:51:45', 3, ''),
(1347, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:53:03', 3, ''),
(1348, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:55:40', 3, ''),
(1349, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:56:13', 3, ''),
(1350, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:57:07', 3, ''),
(1351, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:58:10', 3, ''),
(1352, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '12:59:26', 3, ''),
(1353, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '13:00:38', 3, ''),
(1354, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '13:04:49', 3, ''),
(1355, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '13:05:25', 3, ''),
(1356, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '13:05:49', 3, ''),
(1357, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '13:06:17', 3, ''),
(1358, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '13:13:14', 3, ''),
(1359, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '13:16:31', 3, ''),
(1360, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '13:18:42', 3, ''),
(1361, 'ALTERAÇÃO DO CLIENTE ', '', '2013-09-25', '14:07:20', 3, ''),
(1362, 'ALTERAÇÃO DO CLIENTE ', '', '2013-10-11', '14:31:02', 3, ''),
(1363, 'ALTERAÇÃO DO CLIENTE ', '', '2013-10-11', '14:31:48', 3, ''),
(1364, 'ALTERAÇÃO DO CLIENTE ', '', '2013-10-11', '14:32:06', 3, ''),
(1365, 'ALTERAÇÃO DO CLIENTE ', '', '2013-10-11', '14:35:05', 3, ''),
(1366, 'ALTERAÇÃO DO CLIENTE ', '', '2013-10-11', '15:57:45', 3, ''),
(1367, 'ALTERAÇÃO DO CLIENTE ', '', '2013-10-11', '16:00:00', 3, ''),
(1368, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-29', '11:01:17', 3, ''),
(1369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-29', '15:46:59', 3, ''),
(1370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-29', '15:47:50', 3, ''),
(1371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '03:42:01', 3, ''),
(1372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '03:43:12', 3, ''),
(1373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '03:46:01', 3, ''),
(1374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '03:49:20', 3, ''),
(1375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:00:34', 3, ''),
(1376, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:02:44', 3, ''),
(1377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:04:58', 3, ''),
(1378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:06:47', 3, ''),
(1379, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:10:48', 3, ''),
(1380, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:15:03', 3, ''),
(1381, 'EXCLUSÃO DO LOGIN 206, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'206\'', '2016-03-02', '04:17:08', 3, ''),
(1382, 'EXCLUSÃO DO LOGIN 205, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'205\'', '2016-03-02', '04:17:16', 3, ''),
(1383, 'EXCLUSÃO DO LOGIN 21, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'21\'', '2016-03-02', '04:17:21', 3, ''),
(1384, 'EXCLUSÃO DO LOGIN 22, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'22\'', '2016-03-02', '04:17:25', 3, ''),
(1385, 'EXCLUSÃO DO LOGIN 24, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'24\'', '2016-03-02', '04:17:29', 3, ''),
(1386, 'EXCLUSÃO DO LOGIN 27, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'27\'', '2016-03-02', '04:17:33', 3, ''),
(1387, 'EXCLUSÃO DO LOGIN 29, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'29\'', '2016-03-02', '04:17:37', 3, ''),
(1388, 'EXCLUSÃO DO LOGIN 30, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'30\'', '2016-03-02', '04:17:41', 3, ''),
(1389, 'EXCLUSÃO DO LOGIN 25, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'25\'', '2016-03-02', '04:17:45', 3, ''),
(1390, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:19:36', 3, ''),
(1391, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:42:21', 3, ''),
(1392, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:43:21', 3, ''),
(1393, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:44:05', 3, ''),
(1394, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '04:47:28', 3, ''),
(1395, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'11\'', '2016-03-02', '04:52:36', 3, '');
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`, `url_amigavel`) VALUES
(1396, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'11\'', '2016-03-02', '04:52:37', 3, ''),
(1397, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'12\'', '2016-03-02', '04:53:39', 3, ''),
(1398, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '11:56:34', 3, ''),
(1399, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '11:58:07', 3, ''),
(1400, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '12:08:52', 3, ''),
(1401, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_marcas_produtos WHERE idmarcaproduto = \'1\'', '2016-03-02', '12:11:56', 3, ''),
(1402, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_marcas_produtos WHERE idmarcaproduto = \'2\'', '2016-03-02', '12:12:00', 3, ''),
(1403, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_marcas_produtos WHERE idmarcaproduto = \'3\'', '2016-03-02', '12:12:03', 3, ''),
(1404, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'1\'', '2016-03-02', '12:12:15', 3, ''),
(1405, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'3\'', '2016-03-02', '12:12:19', 3, ''),
(1406, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'4\'', '2016-03-02', '12:12:22', 3, ''),
(1407, 'EXCLUSÃO DO LOGIN 190, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'190\'', '2016-03-02', '12:12:51', 3, ''),
(1408, 'EXCLUSÃO DO LOGIN 19, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'19\'', '2016-03-02', '12:12:55', 3, ''),
(1409, 'EXCLUSÃO DO LOGIN 20, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'20\'', '2016-03-02', '12:12:59', 3, ''),
(1410, 'EXCLUSÃO DO LOGIN 23, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'23\'', '2016-03-02', '12:13:03', 3, ''),
(1411, 'EXCLUSÃO DO LOGIN 26, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'26\'', '2016-03-02', '12:13:06', 3, ''),
(1412, 'EXCLUSÃO DO LOGIN 28, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'28\'', '2016-03-02', '12:13:10', 3, ''),
(1413, 'EXCLUSÃO DO LOGIN 31, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'31\'', '2016-03-02', '12:13:13', 3, ''),
(1414, 'EXCLUSÃO DO LOGIN 33, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'33\'', '2016-03-02', '12:13:16', 3, ''),
(1415, 'EXCLUSÃO DO LOGIN 34, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'34\'', '2016-03-02', '12:13:20', 3, ''),
(1416, 'EXCLUSÃO DO LOGIN 35, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'35\'', '2016-03-02', '12:13:23', 3, ''),
(1417, 'EXCLUSÃO DO LOGIN 36, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'36\'', '2016-03-02', '12:13:26', 3, ''),
(1418, 'EXCLUSÃO DO LOGIN 38, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'38\'', '2016-03-02', '12:13:37', 3, ''),
(1419, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'39\'', '2016-03-02', '12:13:41', 3, ''),
(1420, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'40\'', '2016-03-02', '12:13:44', 3, ''),
(1421, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'41\'', '2016-03-02', '12:13:48', 3, ''),
(1422, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'42\'', '2016-03-02', '12:13:52', 3, ''),
(1423, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'43\'', '2016-03-02', '12:13:56', 3, ''),
(1424, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'44\'', '2016-03-02', '12:13:59', 3, ''),
(1425, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'45\'', '2016-03-02', '12:14:02', 3, ''),
(1426, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'46\'', '2016-03-02', '12:14:06', 3, ''),
(1427, 'EXCLUSÃO DO LOGIN 47, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'47\'', '2016-03-02', '12:14:09', 3, ''),
(1428, 'EXCLUSÃO DO LOGIN 49, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'49\'', '2016-03-02', '12:14:12', 3, ''),
(1429, 'EXCLUSÃO DO LOGIN 50, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'50\'', '2016-03-02', '12:14:16', 3, ''),
(1430, 'EXCLUSÃO DO LOGIN 52, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'52\'', '2016-03-02', '12:14:19', 3, ''),
(1431, 'EXCLUSÃO DO LOGIN 53, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'53\'', '2016-03-02', '12:14:23', 3, ''),
(1432, 'EXCLUSÃO DO LOGIN 54, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'54\'', '2016-03-02', '12:14:26', 3, ''),
(1433, 'EXCLUSÃO DO LOGIN 55, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'55\'', '2016-03-02', '12:14:44', 3, ''),
(1434, 'EXCLUSÃO DO LOGIN 57, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'57\'', '2016-03-02', '12:14:49', 3, ''),
(1435, 'EXCLUSÃO DO LOGIN 59, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'59\'', '2016-03-02', '12:14:52', 3, ''),
(1436, 'EXCLUSÃO DO LOGIN 58, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'58\'', '2016-03-02', '12:14:56', 3, ''),
(1437, 'EXCLUSÃO DO LOGIN 60, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'60\'', '2016-03-02', '12:14:59', 3, ''),
(1438, 'EXCLUSÃO DO LOGIN 62, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'62\'', '2016-03-02', '12:15:03', 3, ''),
(1439, 'EXCLUSÃO DO LOGIN 63, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'63\'', '2016-03-02', '12:15:06', 3, ''),
(1440, 'EXCLUSÃO DO LOGIN 61, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'61\'', '2016-03-02', '12:15:09', 3, ''),
(1441, 'EXCLUSÃO DO LOGIN 64, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'64\'', '2016-03-02', '12:15:13', 3, ''),
(1442, 'EXCLUSÃO DO LOGIN 66, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'66\'', '2016-03-02', '12:15:16', 3, ''),
(1443, 'EXCLUSÃO DO LOGIN 67, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'67\'', '2016-03-02', '12:15:19', 3, ''),
(1444, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'70\'', '2016-03-02', '12:15:23', 3, ''),
(1445, 'EXCLUSÃO DO LOGIN 71, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'71\'', '2016-03-02', '12:15:26', 3, ''),
(1446, 'EXCLUSÃO DO LOGIN 72, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'72\'', '2016-03-02', '12:15:29', 3, ''),
(1447, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'73\'', '2016-03-02', '12:15:33', 3, ''),
(1448, 'EXCLUSÃO DO LOGIN 74, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'74\'', '2016-03-02', '12:15:36', 3, ''),
(1449, 'EXCLUSÃO DO LOGIN 75, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'75\'', '2016-03-02', '12:15:39', 3, ''),
(1450, 'EXCLUSÃO DO LOGIN 76, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'76\'', '2016-03-02', '12:15:43', 3, ''),
(1451, 'EXCLUSÃO DO LOGIN 65, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'65\'', '2016-03-02', '12:15:46', 3, ''),
(1452, 'EXCLUSÃO DO LOGIN 77, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'77\'', '2016-03-02', '12:15:49', 3, ''),
(1453, 'EXCLUSÃO DO LOGIN 78, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'78\'', '2016-03-02', '12:15:53', 3, ''),
(1454, 'EXCLUSÃO DO LOGIN 79, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'79\'', '2016-03-02', '12:15:56', 3, ''),
(1455, 'EXCLUSÃO DO LOGIN 81, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'81\'', '2016-03-02', '12:15:59', 3, ''),
(1456, 'EXCLUSÃO DO LOGIN 82, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'82\'', '2016-03-02', '12:16:02', 3, ''),
(1457, 'EXCLUSÃO DO LOGIN 80, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'80\'', '2016-03-02', '12:16:05', 3, ''),
(1458, 'EXCLUSÃO DO LOGIN 84, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'84\'', '2016-03-02', '12:16:08', 3, ''),
(1459, 'EXCLUSÃO DO LOGIN 83, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'83\'', '2016-03-02', '12:16:11', 3, ''),
(1460, 'EXCLUSÃO DO LOGIN 85, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'85\'', '2016-03-02', '12:16:14', 3, ''),
(1461, 'EXCLUSÃO DO LOGIN 86, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'86\'', '2016-03-02', '12:16:17', 3, ''),
(1462, 'EXCLUSÃO DO LOGIN 87, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'87\'', '2016-03-02', '12:16:21', 3, ''),
(1463, 'EXCLUSÃO DO LOGIN 88, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'88\'', '2016-03-02', '12:16:24', 3, ''),
(1464, 'EXCLUSÃO DO LOGIN 89, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'89\'', '2016-03-02', '12:16:28', 3, ''),
(1465, 'EXCLUSÃO DO LOGIN 90, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'90\'', '2016-03-02', '12:16:31', 3, ''),
(1466, 'EXCLUSÃO DO LOGIN 91, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'91\'', '2016-03-02', '12:16:34', 3, ''),
(1467, 'EXCLUSÃO DO LOGIN 92, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'92\'', '2016-03-02', '12:16:37', 3, ''),
(1468, 'EXCLUSÃO DO LOGIN 95, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'95\'', '2016-03-02', '12:16:41', 3, ''),
(1469, 'EXCLUSÃO DO LOGIN 97, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'97\'', '2016-03-02', '12:16:44', 3, ''),
(1470, 'EXCLUSÃO DO LOGIN 99, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'99\'', '2016-03-02', '12:16:47', 3, ''),
(1471, 'EXCLUSÃO DO LOGIN 100, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'100\'', '2016-03-02', '12:16:51', 3, ''),
(1472, 'EXCLUSÃO DO LOGIN 112, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'112\'', '2016-03-02', '12:16:54', 3, ''),
(1473, 'EXCLUSÃO DO LOGIN 113, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'113\'', '2016-03-02', '12:16:57', 3, ''),
(1474, 'EXCLUSÃO DO LOGIN 114, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'114\'', '2016-03-02', '12:17:00', 3, ''),
(1475, 'EXCLUSÃO DO LOGIN 115, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'115\'', '2016-03-02', '12:17:03', 3, ''),
(1476, 'EXCLUSÃO DO LOGIN 116, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'116\'', '2016-03-02', '12:17:06', 3, ''),
(1477, 'EXCLUSÃO DO LOGIN 96, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'96\'', '2016-03-02', '12:17:10', 3, ''),
(1478, 'EXCLUSÃO DO LOGIN 117, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'117\'', '2016-03-02', '12:17:13', 3, ''),
(1479, 'EXCLUSÃO DO LOGIN 118, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'118\'', '2016-03-02', '12:17:16', 3, ''),
(1480, 'EXCLUSÃO DO LOGIN 119, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'119\'', '2016-03-02', '12:17:19', 3, ''),
(1481, 'EXCLUSÃO DO LOGIN 120, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'120\'', '2016-03-02', '12:17:22', 3, ''),
(1482, 'EXCLUSÃO DO LOGIN 121, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'121\'', '2016-03-02', '12:17:25', 3, ''),
(1483, 'EXCLUSÃO DO LOGIN 56, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'56\'', '2016-03-02', '12:17:30', 3, ''),
(1484, 'EXCLUSÃO DO LOGIN 37, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'37\'', '2016-03-02', '12:17:33', 3, ''),
(1485, 'EXCLUSÃO DO LOGIN 122, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'122\'', '2016-03-02', '12:17:36', 3, ''),
(1486, 'EXCLUSÃO DO LOGIN 123, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'123\'', '2016-03-02', '12:17:39', 3, ''),
(1487, 'EXCLUSÃO DO LOGIN 124, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'124\'', '2016-03-02', '12:17:43', 3, ''),
(1488, 'EXCLUSÃO DO LOGIN 125, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'125\'', '2016-03-02', '12:17:46', 3, ''),
(1489, 'EXCLUSÃO DO LOGIN 126, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'126\'', '2016-03-02', '12:17:49', 3, ''),
(1490, 'EXCLUSÃO DO LOGIN 127, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'127\'', '2016-03-02', '12:17:52', 3, ''),
(1491, 'EXCLUSÃO DO LOGIN 128, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'128\'', '2016-03-02', '12:17:55', 3, ''),
(1492, 'EXCLUSÃO DO LOGIN 130, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'130\'', '2016-03-02', '12:17:58', 3, ''),
(1493, 'EXCLUSÃO DO LOGIN 131, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'131\'', '2016-03-02', '12:18:01', 3, ''),
(1494, 'EXCLUSÃO DO LOGIN 132, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'132\'', '2016-03-02', '12:18:05', 3, ''),
(1495, 'EXCLUSÃO DO LOGIN 93, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'93\'', '2016-03-02', '12:18:08', 3, ''),
(1496, 'EXCLUSÃO DO LOGIN 133, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'133\'', '2016-03-02', '12:18:12', 3, ''),
(1497, 'EXCLUSÃO DO LOGIN 129, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'129\'', '2016-03-02', '12:18:15', 3, ''),
(1498, 'EXCLUSÃO DO LOGIN 134, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'134\'', '2016-03-02', '12:18:18', 3, ''),
(1499, 'EXCLUSÃO DO LOGIN 136, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'136\'', '2016-03-02', '12:18:21', 3, ''),
(1500, 'EXCLUSÃO DO LOGIN 137, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'137\'', '2016-03-02', '12:18:25', 3, ''),
(1501, 'EXCLUSÃO DO LOGIN 138, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'138\'', '2016-03-02', '12:18:28', 3, ''),
(1502, 'EXCLUSÃO DO LOGIN 140, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'140\'', '2016-03-02', '12:18:31', 3, ''),
(1503, 'EXCLUSÃO DO LOGIN 141, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'141\'', '2016-03-02', '12:18:35', 3, ''),
(1504, 'EXCLUSÃO DO LOGIN 143, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'143\'', '2016-03-02', '12:18:38', 3, ''),
(1505, 'EXCLUSÃO DO LOGIN 144, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'144\'', '2016-03-02', '12:18:41', 3, ''),
(1506, 'EXCLUSÃO DO LOGIN 145, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'145\'', '2016-03-02', '12:18:44', 3, ''),
(1507, 'EXCLUSÃO DO LOGIN 146, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'146\'', '2016-03-02', '12:18:47', 3, ''),
(1508, 'EXCLUSÃO DO LOGIN 147, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'147\'', '2016-03-02', '12:18:50', 3, ''),
(1509, 'EXCLUSÃO DO LOGIN 149, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'149\'', '2016-03-02', '12:18:53', 3, ''),
(1510, 'EXCLUSÃO DO LOGIN 150, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'150\'', '2016-03-02', '12:18:57', 3, ''),
(1511, 'EXCLUSÃO DO LOGIN 151, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'151\'', '2016-03-02', '12:19:00', 3, ''),
(1512, 'EXCLUSÃO DO LOGIN 152, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'152\'', '2016-03-02', '12:19:03', 3, ''),
(1513, 'EXCLUSÃO DO LOGIN 153, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'153\'', '2016-03-02', '12:19:06', 3, ''),
(1514, 'EXCLUSÃO DO LOGIN 154, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'154\'', '2016-03-02', '12:19:09', 3, ''),
(1515, 'EXCLUSÃO DO LOGIN 142, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'142\'', '2016-03-02', '12:19:12', 3, ''),
(1516, 'EXCLUSÃO DO LOGIN 156, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'156\'', '2016-03-02', '12:19:15', 3, ''),
(1517, 'EXCLUSÃO DO LOGIN 155, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'155\'', '2016-03-02', '12:19:18', 3, ''),
(1518, 'EXCLUSÃO DO LOGIN 139, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'139\'', '2016-03-02', '12:19:21', 3, ''),
(1519, 'EXCLUSÃO DO LOGIN 157, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'157\'', '2016-03-02', '12:19:24', 3, ''),
(1520, 'EXCLUSÃO DO LOGIN 159, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'159\'', '2016-03-02', '12:19:27', 3, ''),
(1521, 'EXCLUSÃO DO LOGIN 158, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'158\'', '2016-03-02', '12:19:31', 3, ''),
(1522, 'EXCLUSÃO DO LOGIN 161, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'161\'', '2016-03-02', '12:19:33', 3, ''),
(1523, 'EXCLUSÃO DO LOGIN 163, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'163\'', '2016-03-02', '12:19:36', 3, ''),
(1524, 'EXCLUSÃO DO LOGIN 162, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'162\'', '2016-03-02', '12:19:39', 3, ''),
(1525, 'EXCLUSÃO DO LOGIN 165, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'165\'', '2016-03-02', '12:19:42', 3, ''),
(1526, 'EXCLUSÃO DO LOGIN 166, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'166\'', '2016-03-02', '12:19:45', 3, ''),
(1527, 'EXCLUSÃO DO LOGIN 164, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'164\'', '2016-03-02', '12:19:48', 3, ''),
(1528, 'EXCLUSÃO DO LOGIN 135, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'135\'', '2016-03-02', '12:19:51', 3, ''),
(1529, 'EXCLUSÃO DO LOGIN 160, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'160\'', '2016-03-02', '12:19:54', 3, ''),
(1530, 'EXCLUSÃO DO LOGIN 167, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'167\'', '2016-03-02', '12:19:57', 3, ''),
(1531, 'EXCLUSÃO DO LOGIN 168, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'168\'', '2016-03-02', '12:20:00', 3, ''),
(1532, 'EXCLUSÃO DO LOGIN 169, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'169\'', '2016-03-02', '12:20:03', 3, ''),
(1533, 'EXCLUSÃO DO LOGIN 170, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'170\'', '2016-03-02', '12:20:05', 3, ''),
(1534, 'EXCLUSÃO DO LOGIN 171, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'171\'', '2016-03-02', '12:20:08', 3, ''),
(1535, 'EXCLUSÃO DO LOGIN 172, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'172\'', '2016-03-02', '12:20:11', 3, ''),
(1536, 'EXCLUSÃO DO LOGIN 173, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'173\'', '2016-03-02', '12:20:14', 3, ''),
(1537, 'EXCLUSÃO DO LOGIN 174, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'174\'', '2016-03-02', '12:20:17', 3, ''),
(1538, 'EXCLUSÃO DO LOGIN 175, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'175\'', '2016-03-02', '12:20:20', 3, ''),
(1539, 'EXCLUSÃO DO LOGIN 176, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'176\'', '2016-03-02', '12:20:23', 3, ''),
(1540, 'EXCLUSÃO DO LOGIN 177, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'177\'', '2016-03-02', '12:20:26', 3, ''),
(1541, 'EXCLUSÃO DO LOGIN 178, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'178\'', '2016-03-02', '12:20:29', 3, ''),
(1542, 'EXCLUSÃO DO LOGIN 179, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'179\'', '2016-03-02', '12:20:32', 3, ''),
(1543, 'EXCLUSÃO DO LOGIN 180, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'180\'', '2016-03-02', '12:20:35', 3, ''),
(1544, 'EXCLUSÃO DO LOGIN 181, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'181\'', '2016-03-02', '12:20:38', 3, ''),
(1545, 'EXCLUSÃO DO LOGIN 182, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'182\'', '2016-03-02', '12:20:40', 3, ''),
(1546, 'EXCLUSÃO DO LOGIN 183, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'183\'', '2016-03-02', '12:20:43', 3, ''),
(1547, 'EXCLUSÃO DO LOGIN 184, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'184\'', '2016-03-02', '12:20:46', 3, ''),
(1548, 'EXCLUSÃO DO LOGIN 185, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'185\'', '2016-03-02', '12:20:49', 3, ''),
(1549, 'EXCLUSÃO DO LOGIN 186, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'186\'', '2016-03-02', '12:20:51', 3, ''),
(1550, 'EXCLUSÃO DO LOGIN 187, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'187\'', '2016-03-02', '12:20:54', 3, ''),
(1551, 'EXCLUSÃO DO LOGIN 188, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'188\'', '2016-03-02', '12:20:57', 3, ''),
(1552, 'EXCLUSÃO DO LOGIN 189, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'189\'', '2016-03-02', '12:21:00', 3, ''),
(1553, 'EXCLUSÃO DO LOGIN 191, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'191\'', '2016-03-02', '12:21:03', 3, ''),
(1554, 'EXCLUSÃO DO LOGIN 192, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'192\'', '2016-03-02', '12:21:06', 3, ''),
(1555, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'18\'', '2016-03-02', '12:21:09', 3, ''),
(1556, 'EXCLUSÃO DO LOGIN 193, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'193\'', '2016-03-02', '12:21:12', 3, ''),
(1557, 'EXCLUSÃO DO LOGIN 194, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'194\'', '2016-03-02', '12:21:15', 3, ''),
(1558, 'EXCLUSÃO DO LOGIN 195, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'195\'', '2016-03-02', '12:21:17', 3, ''),
(1559, 'EXCLUSÃO DO LOGIN 196, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'196\'', '2016-03-02', '12:21:20', 3, ''),
(1560, 'EXCLUSÃO DO LOGIN 197, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'197\'', '2016-03-02', '12:21:22', 3, ''),
(1561, 'EXCLUSÃO DO LOGIN 198, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'198\'', '2016-03-02', '12:21:25', 3, ''),
(1562, 'EXCLUSÃO DO LOGIN 199, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'199\'', '2016-03-02', '12:21:28', 3, ''),
(1563, 'EXCLUSÃO DO LOGIN 200, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'200\'', '2016-03-02', '12:21:30', 3, ''),
(1564, 'EXCLUSÃO DO LOGIN 201, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'201\'', '2016-03-02', '12:21:33', 3, ''),
(1565, 'EXCLUSÃO DO LOGIN 202, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'202\'', '2016-03-02', '12:21:36', 3, ''),
(1566, 'EXCLUSÃO DO LOGIN 203, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'203\'', '2016-03-02', '12:21:39', 3, ''),
(1567, 'EXCLUSÃO DO LOGIN 204, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'204\'', '2016-03-02', '12:21:48', 3, ''),
(1568, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'2\'', '2016-03-02', '12:22:21', 3, ''),
(1569, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'3\'', '2016-03-02', '12:22:24', 3, ''),
(1570, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'5\'', '2016-03-02', '12:22:27', 3, ''),
(1571, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'8\'', '2016-03-02', '12:22:30', 3, ''),
(1572, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'9\'', '2016-03-02', '12:22:36', 3, ''),
(1573, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'10\'', '2016-03-02', '12:22:39', 3, ''),
(1574, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'16\'', '2016-03-02', '12:22:43', 3, ''),
(1575, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'3\'', '2016-03-02', '12:22:59', 3, ''),
(1576, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'4\'', '2016-03-02', '12:23:02', 3, ''),
(1577, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'5\'', '2016-03-02', '12:23:05', 3, ''),
(1578, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'6\'', '2016-03-02', '12:23:08', 3, ''),
(1579, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'7\'', '2016-03-02', '12:23:11', 3, ''),
(1580, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'8\'', '2016-03-02', '12:23:14', 3, ''),
(1581, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'10\'', '2016-03-02', '12:23:17', 3, ''),
(1582, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'11\'', '2016-03-02', '12:23:20', 3, ''),
(1583, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'12\'', '2016-03-02', '12:23:23', 3, ''),
(1584, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'13\'', '2016-03-02', '12:23:25', 3, ''),
(1585, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'14\'', '2016-03-02', '12:23:28', 3, ''),
(1586, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'15\'', '2016-03-02', '12:23:31', 3, ''),
(1587, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '12:33:48', 3, ''),
(1588, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '12:34:06', 3, ''),
(1589, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'6\'', '2016-03-02', '13:56:15', 3, ''),
(1590, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'7\'', '2016-03-02', '13:56:19', 3, ''),
(1591, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'9\'', '2016-03-02', '13:56:22', 3, ''),
(1592, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-03', '02:32:04', 3, ''),
(1593, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-03', '02:32:43', 3, ''),
(1594, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-03', '02:33:05', 3, ''),
(1595, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-04', '04:28:15', 3, ''),
(1596, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-04', '04:41:28', 3, ''),
(1597, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '04:42:58', 3, ''),
(1598, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '04:45:14', 3, ''),
(1599, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-04', '04:47:25', 3, ''),
(1600, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-04', '04:47:53', 3, ''),
(1601, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '04:49:09', 3, ''),
(1602, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '04:50:14', 3, ''),
(1603, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '04:51:27', 3, ''),
(1604, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '04:51:59', 3, ''),
(1605, 'EXCLUSÃO DO LOGIN 209, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'209\'', '2016-03-04', '04:52:38', 3, ''),
(1606, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '04:58:13', 3, ''),
(1607, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '05:01:44', 3, ''),
(1608, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '05:09:29', 3, ''),
(1609, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '05:13:37', 3, ''),
(1610, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '05:22:17', 3, ''),
(1611, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-04', '05:23:30', 3, ''),
(1612, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-04', '05:26:44', 3, ''),
(1613, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '05:29:17', 3, ''),
(1614, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '05:32:41', 3, ''),
(1615, 'CADASTRO DO CLIENTE ', '', '2016-03-04', '05:35:34', 3, ''),
(1616, 'EXCLUSÃO DO LOGIN 218, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'218\'', '2016-03-04', '15:28:55', 3, ''),
(1617, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '18:35:34', 3, ''),
(1618, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '18:42:00', 3, ''),
(1619, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '18:43:19', 3, ''),
(1620, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '18:56:22', 3, ''),
(1621, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '18:58:44', 3, ''),
(1622, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '19:00:52', 3, ''),
(1623, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:02:56', 3, ''),
(1624, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:04:07', 3, ''),
(1625, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:04:31', 3, ''),
(1626, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:05:04', 3, ''),
(1627, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:05:23', 3, ''),
(1628, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:06:01', 3, ''),
(1629, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:06:28', 3, ''),
(1630, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:07:17', 3, ''),
(1631, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '19:16:00', 3, ''),
(1632, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '17:29:26', 3, ''),
(1633, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-10', '09:57:45', 3, ''),
(1634, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-10', '11:05:05', 3, ''),
(1635, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '16:34:25', 3, ''),
(1636, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '16:55:43', 3, ''),
(1637, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '16:59:28', 3, ''),
(1638, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '16:59:49', 3, ''),
(1639, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '17:31:21', 3, ''),
(1640, 'ALTERAÇÃO DO LOGIN 3', 'UPDATE tb_logins SET nome = \'junior\', email = \'atendimento.sites@homewebbrasil.com.br\', id_grupologin = \'1\' WHERE idlogin = \'3\'', '2016-04-13', '17:51:49', 3, ''),
(1641, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '18:12:03', 3, ''),
(1642, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '18:20:29', 3, ''),
(1643, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '18:22:02', 3, ''),
(1644, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '20:00:14', 3, ''),
(1645, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '01:26:59', 3, ''),
(1646, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '02:02:58', 3, ''),
(1647, 'CADASTRO DO CLIENTE ', '', '2016-04-14', '02:08:22', 3, ''),
(1648, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '02:11:12', 3, ''),
(1649, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '02:13:49', 3, ''),
(1650, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '02:18:26', 3, ''),
(1651, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '10:39:02', 3, ''),
(1652, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '10:40:21', 3, ''),
(1653, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '11:13:34', 3, ''),
(1654, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '11:18:58', 3, ''),
(1655, 'CADASTRO DO CLIENTE ', '', '2016-04-14', '11:26:18', 3, ''),
(1656, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '11:29:35', 3, ''),
(1657, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '11:30:34', 3, ''),
(1658, 'CADASTRO DO CLIENTE ', '', '2016-04-14', '11:34:23', 3, ''),
(1659, 'CADASTRO DO CLIENTE ', '', '2016-04-14', '11:34:41', 3, ''),
(1660, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '11:44:50', 3, ''),
(1661, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '11:45:32', 3, ''),
(1662, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '11:55:32', 3, ''),
(1663, 'CADASTRO DO CLIENTE ', '', '2016-04-14', '12:02:32', 3, ''),
(1664, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '12:06:29', 3, ''),
(1665, 'CADASTRO DO CLIENTE ', '', '2016-04-14', '12:40:15', 3, ''),
(1666, 'CADASTRO DO CLIENTE ', '', '2016-04-14', '12:47:06', 3, ''),
(1667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '12:51:20', 3, ''),
(1668, 'EXCLUSÃO DO LOGIN 211, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'211\'', '2016-04-14', '13:04:32', 3, ''),
(1669, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '13:35:34', 3, ''),
(1670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-14', '13:36:02', 3, ''),
(1671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:39:43', 3, ''),
(1672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:40:01', 3, ''),
(1673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:40:07', 3, ''),
(1674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:44:45', 3, ''),
(1675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:46:55', 3, ''),
(1676, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:47:14', 3, ''),
(1677, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:48:40', 3, ''),
(1678, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:49:07', 3, ''),
(1679, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:49:42', 3, ''),
(1680, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '02:53:38', 3, ''),
(1681, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:01:01', 3, ''),
(1682, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:01:25', 3, ''),
(1683, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:02:02', 3, ''),
(1684, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:02:27', 3, ''),
(1685, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:02:49', 3, ''),
(1686, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:03:16', 3, ''),
(1687, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:04:07', 3, ''),
(1688, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:05:34', 3, ''),
(1689, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:06:08', 3, ''),
(1690, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '03:18:08', 3, ''),
(1691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:03:36', 3, ''),
(1692, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'4\'', '2016-04-15', '04:04:05', 3, ''),
(1693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:06:18', 3, ''),
(1694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:06:29', 3, ''),
(1695, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'6\'', '2016-04-15', '04:06:49', 3, ''),
(1696, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:08:59', 3, ''),
(1697, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:09:53', 3, ''),
(1698, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:10:27', 3, ''),
(1699, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:10:47', 3, ''),
(1700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:10:57', 3, ''),
(1701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:13:00', 3, ''),
(1702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:13:21', 3, ''),
(1703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:14:07', 3, ''),
(1704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:14:34', 3, ''),
(1705, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:14:52', 3, ''),
(1706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:22:16', 3, ''),
(1707, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:31:23', 3, ''),
(1708, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:34:02', 3, ''),
(1709, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:35:46', 3, ''),
(1710, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:46:00', 3, ''),
(1711, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '04:49:12', 3, ''),
(1712, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '12:13:10', 3, ''),
(1713, 'CADASTRO DO CLIENTE ', '', '2016-04-15', '12:19:08', 3, ''),
(1714, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '12:20:08', 3, ''),
(1715, 'CADASTRO DO CLIENTE ', '', '2016-04-15', '12:22:53', 3, ''),
(1716, 'CADASTRO DO CLIENTE ', '', '2016-04-15', '12:23:55', 3, ''),
(1717, 'EXCLUSÃO DO LOGIN 225, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'225\'', '2016-04-15', '12:24:35', 3, ''),
(1718, 'CADASTRO DO CLIENTE ', '', '2016-04-15', '12:30:02', 3, ''),
(1719, 'EXCLUSÃO DO LOGIN 227, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'227\'', '2016-04-15', '12:30:23', 3, ''),
(1720, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '12:40:17', 3, ''),
(1721, 'CADASTRO DO CLIENTE ', '', '2016-04-15', '14:42:36', 3, ''),
(1722, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '14:47:17', 3, ''),
(1723, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-15', '15:10:45', 3, ''),
(1724, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '16:34:08', 3, ''),
(1725, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '13:28:53', 3, ''),
(1726, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '16:51:15', 3, ''),
(1727, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '15:28:06', 3, ''),
(1728, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '17:28:52', 3, ''),
(1729, 'EXCLUSÃO DO LOGIN 5, NOME: Danilo, Email: vendas@disklimp.com', 'DELETE FROM tb_logins WHERE idlogin = \'5\'', '2016-05-23', '16:24:30', 3, ''),
(1730, 'EXCLUSÃO DO LOGIN 2, NOME: David Leandro dos Santos, Email: design.davidleandro@gmail.com', 'DELETE FROM tb_logins WHERE idlogin = \'2\'', '2016-05-23', '16:25:30', 3, ''),
(1731, 'EXCLUSÃO DO LOGIN 4, NOME: Mas Midia, Email: contato@masmdiaia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'4\'', '2016-05-23', '16:25:40', 3, ''),
(1732, 'CADASTRO DE GRUPO Cliente', 'INSERT INTO	tb_grupos_logins\n					(nome)\n					VALUES\n					(\'Cliente\')', '2016-05-26', '11:22:29', 3, ''),
(1733, 'CADASTRO DO LOGIN So Bandeiras', 'INSERT INTO	tb_logins\n					(nome, senha, email, id_grupologin)\n					VALUES\n					(\'So Bandeiras\', \'1aece1bc35fd4c1c0155057d738f21b0\', \'contato.sobandeiras@gmail.com\', \'10\')', '2016-05-26', '11:24:02', 3, ''),
(1734, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-26', '11:39:32', 4, ''),
(1735, 'EXCLUSÃO DO LOGIN 4, NOME: So Bandeiras, Email: contato.sobandeiras@gmail.com', 'DELETE FROM tb_logins WHERE idlogin = \'4\'', '2016-06-08', '17:47:29', 3, ''),
(1736, 'CADASTRO DO LOGIN contato.sobandeiras@gmail.com', 'INSERT INTO	tb_logins\n					(nome, senha, email, id_grupologin)\n					VALUES\n					(\'contato.sobandeiras@gmail.com\', \'bdb8d2441c4a5fe26ff38daf81ac1101\', \'contato.sobandeiras@gmail.com\', \'10\')', '2016-06-08', '17:48:30', 3, ''),
(1737, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-10', '14:39:10', 3, ''),
(1738, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-10', '14:40:26', 3, ''),
(1739, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-05', '09:54:30', 5, ''),
(1740, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-05', '10:00:04', 5, ''),
(1741, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '09:59:40', 5, ''),
(1742, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '10:02:17', 5, ''),
(1743, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '10:16:17', 5, ''),
(1744, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '10:18:59', 5, ''),
(1745, 'EXCLUSÃO DO LOGIN 230, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'230\'', '2016-07-12', '11:10:53', 3, ''),
(1746, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '11:53:40', 5, ''),
(1747, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '11:56:29', 5, ''),
(1748, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '11:58:20', 5, ''),
(1749, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '12:00:08', 5, ''),
(1750, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '13:52:24', 5, ''),
(1751, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '14:06:37', 5, ''),
(1752, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '14:14:02', 5, ''),
(1753, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'8\'', '2016-07-12', '14:16:16', 5, ''),
(1754, 'EXCLUSÃO DO LOGIN 237, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'237\'', '2016-07-12', '14:19:09', 5, ''),
(1755, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '14:24:27', 5, ''),
(1756, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '14:26:05', 5, ''),
(1757, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '14:28:29', 5, ''),
(1758, 'CADASTRO DO CLIENTE ', '', '2016-07-12', '14:30:11', 5, ''),
(1759, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'9\'', '2016-07-12', '14:41:37', 5, ''),
(1760, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'9\'', '2016-07-12', '16:41:28', 5, ''),
(1761, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '15:59:46', 13, ''),
(1762, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '16:14:34', 13, ''),
(1763, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '16:44:27', 13, ''),
(1764, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '16:50:03', 13, ''),
(1765, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '16:51:16', 13, ''),
(1766, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '16:51:44', 13, ''),
(1767, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '16:53:15', 13, ''),
(1768, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '19:20:09', 13, ''),
(1769, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '19:26:58', 13, ''),
(1770, 'CADASTRO DO CLIENTE ', '', '2016-08-23', '19:29:19', 13, ''),
(1771, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '19:38:20', 13, ''),
(1772, 'CADASTRO DO CLIENTE ', '', '2016-08-23', '19:45:09', 13, ''),
(1773, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '19:46:45', 13, ''),
(1774, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '19:47:05', 13, ''),
(1775, 'CADASTRO DO CLIENTE ', '', '2016-08-23', '19:47:52', 13, ''),
(1776, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '20:31:25', 13, ''),
(1777, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '20:47:25', 13, ''),
(1778, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '21:02:42', 13, ''),
(1779, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-23', '21:03:37', 13, ''),
(1780, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '08:51:12', 13, ''),
(1781, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '08:51:44', 13, ''),
(1782, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '09:41:12', 13, ''),
(1783, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '09:42:02', 13, ''),
(1784, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '10:05:11', 13, ''),
(1785, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '11:14:51', 13, ''),
(1786, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '11:16:05', 13, ''),
(1787, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '11:55:18', 13, ''),
(1788, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '12:02:41', 13, ''),
(1789, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '12:03:48', 13, ''),
(1790, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '15:06:25', 13, ''),
(1791, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '15:17:55', 13, ''),
(1792, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-24', '15:17:55', 13, ''),
(1793, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '08:57:40', 13, ''),
(1794, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '09:36:47', 13, ''),
(1795, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '09:40:15', 13, ''),
(1796, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '09:41:03', 13, ''),
(1797, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '10:02:58', 13, ''),
(1798, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '10:06:21', 13, ''),
(1799, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:57:11', 13, ''),
(1800, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-29', '17:13:49', 13, ''),
(1801, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-29', '17:45:41', 13, ''),
(1802, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-29', '20:12:34', 13, ''),
(1803, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-30', '09:43:23', 13, ''),
(1804, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-30', '11:06:02', 13, ''),
(1805, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-30', '13:41:05', 13, ''),
(1806, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:07:35', 1, ''),
(1807, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:09:03', 1, ''),
(1808, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:12:32', 1, ''),
(1809, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:13:56', 1, ''),
(1810, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:14:08', 1, ''),
(1811, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:14:48', 1, ''),
(1812, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:17:50', 1, ''),
(1813, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:21:56', 1, ''),
(1814, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:23:32', 1, ''),
(1815, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:26:00', 1, ''),
(1816, 'EXCLUSÃO DO LOGIN 220, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'220\'', '2016-09-01', '18:26:33', 1, ''),
(1817, 'EXCLUSÃO DO LOGIN 221, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'221\'', '2016-09-01', '18:26:36', 1, ''),
(1818, 'EXCLUSÃO DO LOGIN 223, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'223\'', '2016-09-01', '18:26:40', 1, ''),
(1819, 'EXCLUSÃO DO LOGIN 226, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'226\'', '2016-09-01', '18:26:43', 1, ''),
(1820, 'EXCLUSÃO DO LOGIN 222, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'222\'', '2016-09-01', '18:26:46', 1, ''),
(1821, 'EXCLUSÃO DO LOGIN 213, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'213\'', '2016-09-01', '18:26:50', 1, ''),
(1822, 'EXCLUSÃO DO LOGIN 212, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'212\'', '2016-09-01', '18:26:53', 1, ''),
(1823, 'EXCLUSÃO DO LOGIN 224, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'224\'', '2016-09-01', '18:26:56', 1, ''),
(1824, 'EXCLUSÃO DO LOGIN 228, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'228\'', '2016-09-01', '18:26:59', 1, ''),
(1825, 'EXCLUSÃO DO LOGIN 218, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'218\'', '2016-09-01', '18:27:02', 1, ''),
(1826, 'EXCLUSÃO DO LOGIN 215, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'215\'', '2016-09-01', '18:27:05', 1, ''),
(1827, 'EXCLUSÃO DO LOGIN 217, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'217\'', '2016-09-01', '18:27:07', 1, ''),
(1828, 'EXCLUSÃO DO LOGIN 216, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'216\'', '2016-09-01', '18:27:10', 1, ''),
(1829, 'EXCLUSÃO DO LOGIN 214, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'214\'', '2016-09-01', '18:27:13', 1, ''),
(1830, 'EXCLUSÃO DO LOGIN 210, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'210\'', '2016-09-01', '18:27:16', 1, ''),
(1831, 'EXCLUSÃO DO LOGIN 208, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'208\'', '2016-09-01', '18:27:19', 1, ''),
(1832, 'EXCLUSÃO DO LOGIN 207, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'207\'', '2016-09-01', '18:27:22', 1, ''),
(1833, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'16\'', '2016-09-01', '18:27:25', 1, ''),
(1834, 'EXCLUSÃO DO LOGIN 229, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'229\'', '2016-09-01', '18:27:28', 1, ''),
(1835, 'EXCLUSÃO DO LOGIN 231, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'231\'', '2016-09-01', '18:27:30', 1, ''),
(1836, 'EXCLUSÃO DO LOGIN 232, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'232\'', '2016-09-01', '18:27:33', 1, ''),
(1837, 'EXCLUSÃO DO LOGIN 233, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'233\'', '2016-09-01', '18:27:36', 1, ''),
(1838, 'EXCLUSÃO DO LOGIN 234, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'234\'', '2016-09-01', '18:27:38', 1, ''),
(1839, 'EXCLUSÃO DO LOGIN 235, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'235\'', '2016-09-01', '18:27:41', 1, ''),
(1840, 'EXCLUSÃO DO LOGIN 236, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'236\'', '2016-09-01', '18:27:44', 1, ''),
(1841, 'EXCLUSÃO DO LOGIN 238, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'238\'', '2016-09-01', '18:27:46', 1, ''),
(1842, 'EXCLUSÃO DO LOGIN 239, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'239\'', '2016-09-01', '18:27:49', 1, ''),
(1843, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'5\'', '2016-09-01', '18:28:04', 1, ''),
(1844, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'7\'', '2016-09-01', '18:28:07', 1, ''),
(1845, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-01', '18:28:18', 1, ''),
(1846, 'DESATIVOU O LOGIN 10', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'10\'', '2016-09-08', '03:24:37', 3, ''),
(1847, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '03:47:35', 3, ''),
(1848, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '03:51:17', 3, ''),
(1849, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '03:51:43', 3, ''),
(1850, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '03:53:50', 3, ''),
(1851, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '03:54:26', 3, ''),
(1852, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '03:59:32', 3, ''),
(1853, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '04:00:21', 3, ''),
(1854, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '04:03:11', 3, ''),
(1855, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '04:11:51', 3, ''),
(1856, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-08', '04:13:40', 3, ''),
(1857, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '14:52:11', 3, ''),
(1858, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '14:54:15', 3, ''),
(1859, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '14:56:06', 3, ''),
(1860, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '14:57:30', 3, ''),
(1861, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '14:59:12', 3, ''),
(1862, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '14:59:30', 3, ''),
(1863, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '14:59:56', 3, ''),
(1864, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:00:45', 3, ''),
(1865, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:03:08', 3, ''),
(1866, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:03:47', 3, ''),
(1867, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:04:21', 3, ''),
(1868, 'EXCLUSÃO DO LOGIN 223, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'223\'', '2016-09-09', '15:05:10', 3, ''),
(1869, 'EXCLUSÃO DO LOGIN 226, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'226\'', '2016-09-09', '15:05:14', 3, ''),
(1870, 'EXCLUSÃO DO LOGIN 222, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'222\'', '2016-09-09', '15:05:18', 3, ''),
(1871, 'EXCLUSÃO DO LOGIN 213, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'213\'', '2016-09-09', '15:05:22', 3, ''),
(1872, 'EXCLUSÃO DO LOGIN 212, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'212\'', '2016-09-09', '15:05:25', 3, ''),
(1873, 'EXCLUSÃO DO LOGIN 224, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'224\'', '2016-09-09', '15:05:28', 3, ''),
(1874, 'EXCLUSÃO DO LOGIN 228, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'228\'', '2016-09-09', '15:05:31', 3, ''),
(1875, 'EXCLUSÃO DO LOGIN 218, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'218\'', '2016-09-09', '15:05:34', 3, ''),
(1876, 'EXCLUSÃO DO LOGIN 215, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'215\'', '2016-09-09', '15:05:38', 3, '');
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`, `url_amigavel`) VALUES
(1877, 'EXCLUSÃO DO LOGIN 217, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'217\'', '2016-09-09', '15:05:41', 3, ''),
(1878, 'EXCLUSÃO DO LOGIN 216, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'216\'', '2016-09-09', '15:05:45', 3, ''),
(1879, 'EXCLUSÃO DO LOGIN 214, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'214\'', '2016-09-09', '15:05:48', 3, ''),
(1880, 'EXCLUSÃO DO LOGIN 210, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'210\'', '2016-09-09', '15:05:52', 3, ''),
(1881, 'EXCLUSÃO DO LOGIN 208, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'208\'', '2016-09-09', '15:05:55', 3, ''),
(1882, 'EXCLUSÃO DO LOGIN 207, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'207\'', '2016-09-09', '15:05:58', 3, ''),
(1883, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'16\'', '2016-09-09', '15:06:01', 3, ''),
(1884, 'EXCLUSÃO DO LOGIN 229, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'229\'', '2016-09-09', '15:06:04', 3, ''),
(1885, 'EXCLUSÃO DO LOGIN 231, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'231\'', '2016-09-09', '15:06:07', 3, ''),
(1886, 'EXCLUSÃO DO LOGIN 232, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'232\'', '2016-09-09', '15:06:10', 3, ''),
(1887, 'EXCLUSÃO DO LOGIN 233, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'233\'', '2016-09-09', '15:06:13', 3, ''),
(1888, 'EXCLUSÃO DO LOGIN 234, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'234\'', '2016-09-09', '15:06:16', 3, ''),
(1889, 'EXCLUSÃO DO LOGIN 235, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'235\'', '2016-09-09', '15:06:19', 3, ''),
(1890, 'EXCLUSÃO DO LOGIN 236, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'236\'', '2016-09-09', '15:06:25', 3, ''),
(1891, 'EXCLUSÃO DO LOGIN 238, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'238\'', '2016-09-09', '15:06:28', 3, ''),
(1892, 'EXCLUSÃO DO LOGIN 239, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'239\'', '2016-09-09', '15:06:31', 3, ''),
(1893, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'2\'', '2016-09-09', '15:06:38', 3, ''),
(1894, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'1\'', '2016-09-09', '15:06:41', 3, ''),
(1895, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_categoria_servicos WHERE idcategoriaservico = \'8\'', '2016-09-09', '15:06:45', 3, ''),
(1896, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:10:03', 3, ''),
(1897, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:13:23', 3, ''),
(1898, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:14:10', 3, ''),
(1899, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:15:35', 3, ''),
(1900, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:16:30', 3, ''),
(1901, 'CADASTRO DO CLIENTE ', '', '2016-09-09', '15:19:14', 3, ''),
(1902, 'CADASTRO DO CLIENTE ', '', '2016-09-09', '15:19:58', 3, ''),
(1903, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:20:22', 3, ''),
(1904, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:22:38', 3, ''),
(1905, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '15:23:13', 3, ''),
(1906, 'CADASTRO DO CLIENTE ', '', '2016-09-09', '15:25:19', 3, ''),
(1907, 'CADASTRO DO CLIENTE ', '', '2016-09-09', '15:26:11', 3, ''),
(1908, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'4\'', '2016-09-09', '15:58:18', 3, ''),
(1909, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-09-09', '15:58:22', 3, ''),
(1910, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'4\'', '2016-09-09', '16:20:28', 3, ''),
(1911, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:31:23', 3, ''),
(1912, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:33:07', 3, ''),
(1913, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:35:44', 3, ''),
(1914, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:38:20', 3, ''),
(1915, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:40:32', 3, ''),
(1916, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:42:07', 3, ''),
(1917, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:43:36', 3, ''),
(1918, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'18\'', '2016-09-09', '16:43:55', 3, ''),
(1919, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'13\'', '2016-09-09', '16:43:59', 3, ''),
(1920, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'11\'', '2016-09-09', '16:44:03', 3, ''),
(1921, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'14\'', '2016-09-09', '16:44:08', 3, ''),
(1922, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'17\'', '2016-09-09', '16:44:12', 3, ''),
(1923, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'15\'', '2016-09-09', '16:44:16', 3, ''),
(1924, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'12\'', '2016-09-09', '16:44:19', 3, ''),
(1925, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'9\'', '2016-09-09', '16:44:24', 3, ''),
(1926, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'16\'', '2016-09-09', '16:44:27', 3, ''),
(1927, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:46:21', 3, ''),
(1928, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '16:48:32', 3, ''),
(1929, 'ATIVOU O LOGIN 10', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'10\'', '2016-09-09', '17:55:52', 3, ''),
(1930, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:01:43', 3, ''),
(1931, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:03:14', 3, ''),
(1932, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:03:38', 3, ''),
(1933, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:04:00', 3, ''),
(1934, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:05:14', 3, ''),
(1935, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:05:14', 3, ''),
(1936, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:06:02', 3, ''),
(1937, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:06:20', 3, ''),
(1938, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:06:58', 3, ''),
(1939, 'CADASTRO DO CLIENTE ', '', '2016-09-09', '18:08:33', 3, ''),
(1940, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:17:20', 3, ''),
(1941, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:17:44', 3, ''),
(1942, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:22:47', 3, ''),
(1943, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:23:07', 3, ''),
(1944, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:23:28', 3, ''),
(1945, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:23:49', 3, ''),
(1946, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:24:10', 3, ''),
(1947, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-09', '18:24:24', 3, ''),
(1948, 'EXCLUSÃO DO LOGIN 242, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'242\'', '2016-09-09', '18:24:54', 3, ''),
(1949, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-09-09', '18:35:32', 3, ''),
(1950, 'DESATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'7\'', '2016-09-09', '19:09:07', 3, ''),
(1951, 'ATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'7\'', '2016-09-10', '21:47:39', 3, ''),
(1952, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-10', '22:00:41', 3, ''),
(1953, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-11', '13:09:40', 3, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_marcas_produtos`
--

CREATE TABLE `tb_marcas_produtos` (
  `idmarcaproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(45) NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `tumb_imagem` varchar(45) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_marcas_produtos`
--

INSERT INTO `tb_marcas_produtos` (`idmarcaproduto`, `titulo`, `imagem`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `tumb_imagem`, `url_amigavel`) VALUES
(1, 'Tamanhos', '0703201607008750907236.jpg', '', '', '', 'SIM', 0, '', 'tamanhos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_marcas_servicos`
--

CREATE TABLE `tb_marcas_servicos` (
  `idmarcaservico` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(45) NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `tumb_imagem` varchar(45) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_marcas_servicos`
--

INSERT INTO `tb_marcas_servicos` (`idmarcaservico`, `titulo`, `imagem`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `tumb_imagem`, `url_amigavel`) VALUES
(1, 'Tamanhos', '0703201607008750907236.jpg', '', '', '', 'SIM', 0, '', 'tamanhos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_modulos_paginas`
--

CREATE TABLE `tb_modulos_paginas` (
  `idmodulopagina` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `icone_imagem` varchar(45) DEFAULT NULL,
  `ativo` char(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_modulos_paginas`
--

INSERT INTO `tb_modulos_paginas` (`idmodulopagina`, `nome`, `icone_imagem`, `ativo`, `url_amigavel`) VALUES
(1, 'Grupos', NULL, 'SIM', ''),
(2, 'Logins', NULL, 'SIM', ''),
(28, 'Texto Empresa', NULL, 'SIM', ''),
(29, 'Categorias Produtos', NULL, 'SIM', ''),
(30, 'Marcas Produtos', NULL, 'SIM', ''),
(31, 'Produtos', NULL, 'SIM', ''),
(32, 'Categorias Serviços', NULL, 'SIM', ''),
(33, 'Configuração Geral Site', NULL, 'SIM', ''),
(34, 'Serviços', NULL, 'SIM', ''),
(35, 'Dicas', NULL, 'SIM', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_paginas`
--

CREATE TABLE `tb_paginas` (
  `idpagina` int(11) NOT NULL,
  `pagina` varchar(255) DEFAULT NULL,
  `imagem` varchar(45) DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `exibir_menu` varchar(3) DEFAULT 'SIM',
  `descricao` varchar(255) DEFAULT NULL,
  `id_modulopagina` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_paginas`
--

INSERT INTO `tb_paginas` (`idpagina`, `pagina`, `imagem`, `label`, `exibir_menu`, `descricao`, `id_modulopagina`, `url_amigavel`) VALUES
(148, '/admin/grupo/altera.php', NULL, 'Alterar', 'NAO', NULL, 1, ''),
(149, '/admin/grupo/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 1, ''),
(150, '/admin/grupo/exclui.php', NULL, 'Excluir', 'NAO', NULL, 1, ''),
(151, '/admin/grupo/lista.php', NULL, 'Listar', 'SIM', NULL, 1, ''),
(152, '/admin/grupo/permissoes.php', NULL, 'Permissões', 'NAO', NULL, 1, ''),
(153, '/admin/grupo/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 1, ''),
(154, '/admin/login/altera.php', NULL, 'Alterar', 'NAO', NULL, 2, ''),
(155, '/admin/login/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 2, ''),
(156, '/admin/login/exclui.php', NULL, 'Excluir', 'NAO', NULL, 2, ''),
(157, '/admin/login/lista.php', NULL, 'Listar', 'SIM', NULL, 2, ''),
(158, '/admin/login/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 2, ''),
(159, '/admin/empresa/altera.php', NULL, 'Alterar', 'NAO', NULL, 28, ''),
(160, '/admin/empresa/cadastra.php', NULL, 'Cadastrar', 'NAO', NULL, 28, ''),
(161, '/admin/empresa/exclui.php', NULL, 'Excluir', 'NAO', NULL, 28, ''),
(162, '/admin/empresa/lista.php', NULL, 'Listar', 'SIM', NULL, 28, ''),
(163, '/admin/empresa/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 28, ''),
(164, '/admin/empresa/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 28, ''),
(165, '/admin/categorias-produtos/altera.php', NULL, 'Alterar', 'NAO', NULL, 29, ''),
(166, '/admin/categorias-produtos/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 29, ''),
(167, '/admin/categorias-produtos/exclui.php', NULL, 'Excluir', 'NAO', NULL, 29, ''),
(168, '/admin/categorias-produtos/lista.php', NULL, 'Listar', 'SIM', NULL, 29, ''),
(169, '/admin/categorias-produtos/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 29, ''),
(170, '/admin/categorias-produtos/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 29, ''),
(171, '/admin/marcas-produtos/altera.php', NULL, 'Alterar', 'NAO', NULL, 30, ''),
(172, '/admin/marcas-produtos/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 30, ''),
(173, '/admin/marcas-produtos/exclui.php', NULL, 'Excluir', 'NAO', NULL, 30, ''),
(174, '/admin/marcas-produtos/lista.php', NULL, 'Listar', 'SIM', NULL, 30, ''),
(175, '/admin/marcas-produtos/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 30, ''),
(176, '/admin/marcas-produtos/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 30, ''),
(177, '/admin/produtos/altera.php', NULL, 'Alterar', 'NAO', NULL, 31, ''),
(178, '/admin/produtos/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 31, ''),
(179, '/admin/produtos/exclui.php', NULL, 'Excluir', 'NAO', NULL, 31, ''),
(180, '/admin/produtos/lista.php', NULL, 'Listar', 'SIM', NULL, 31, ''),
(181, '/admin/produtos/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 31, ''),
(182, '/admin/produtos/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 31, ''),
(183, '/admin/categorias-servicos/altera.php', NULL, 'Alterar', 'NAO', NULL, 32, ''),
(184, '/admin/categorias-servicos/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 32, ''),
(185, '/admin/categorias-servicos/exclui.php', NULL, 'Excluir', 'NAO', NULL, 32, ''),
(186, '/admin/categorias-servicos/lista.php', NULL, 'Listar', 'SIM', NULL, 32, ''),
(187, '/admin/categorias-servicos/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 32, ''),
(188, '/admin/categorias-servicos/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 32, ''),
(189, '/admin/info-pagina/altera.php', NULL, 'Alterar', 'NAO', NULL, 33, ''),
(190, '/admin/info-pagina/lista.php', NULL, 'Listar', 'SIM', NULL, 33, ''),
(191, '/admin/servicos/altera.php', NULL, 'Alterar', 'NAO', NULL, 34, ''),
(192, '/admin/servicos/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 34, ''),
(193, '/admin/servicos/exclui.php', NULL, 'Excluir', 'NAO', NULL, 34, ''),
(194, '/admin/servicos/lista.php', NULL, 'Listar', 'SIM', NULL, 34, ''),
(195, '/admin/servicos/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 34, ''),
(196, '/admin/servicos/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 34, ''),
(197, '/admin/dicas/altera.php', NULL, 'Alterar', 'NAO', NULL, 35, ''),
(198, '/admin/dicas/cadastra.php', NULL, 'Cadastrar', 'SIM', NULL, 35, ''),
(199, '/admin/dicas/exclui.php', NULL, 'Excluir', 'NAO', NULL, 35, ''),
(200, '/admin/dicas/lista.php', NULL, 'Listar', 'SIM', NULL, 35, ''),
(201, '/admin/dicas/envia_imagens.php', NULL, 'Envia imagens', 'NAO', NULL, 35, ''),
(202, '/admin/dicas/ativa_desativa.php', NULL, 'Ativar e desativar', 'NAO', NULL, 35, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_parceiros`
--

CREATE TABLE `tb_parceiros` (
  `idparceiro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `img_tumb` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_parceiros`
--

INSERT INTO `tb_parceiros` (`idparceiro`, `titulo`, `descricao`, `imagem`, `img_tumb`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(1, 'TRW', '', '0809201604133360017509.jpg', '2308201609031276380797.png', 'SIM', 0, 'trw', '', '', '', 'http://www.uol.com.br'),
(2, 'LINDE', '', '0909201604355875926183.jpg', '2408201608511403733795.png', 'SIM', 0, 'linde', '', '', '', ''),
(3, 'VISTEON', '', '0909201604313062138280.jpg', '2408201608511159528788.png', 'SIM', 0, 'visteon', '', '', '', 'http://www.uol.com.br'),
(4, 'HYVA', NULL, '0909201604481343985684.jpg', '2308201609031276380797.png', 'SIM', 0, 'hyva', '', '', '', ''),
(5, 'JTEKT', NULL, '0909201604382012650267.jpg', '2308201609031276380797.png', 'SIM', 0, 'jtekt', '', '', '', 'http://www.uol.com.br'),
(6, 'SAUER DANFOSS', NULL, '0909201604403604908486.jpg', '2308201609031276380797.png', 'SIM', 0, 'sauer-danfoss', '', '', '', ''),
(7, 'PARKER', NULL, '0909201604426742028184.jpg', '2308201609031276380797.png', 'SIM', 0, 'parker', '', '', '', 'http://www.uol.com.br'),
(8, 'DHB', NULL, '0909201604436289569718.jpg', '2308201609031276380797.png', 'SIM', 0, 'dhb', '', '', '', ''),
(10, 'BOSCH REXROTH', NULL, '0909201604465781340110.jpg', '2308201609031276380797.png', 'SIM', 0, 'bosch-rexroth', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `id_marcaproduto` int(10) UNSIGNED NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(45) NOT NULL DEFAULT 'produto.jpg',
  `preco` double NOT NULL,
  `descricao` longtext NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `id_marcaproduto`, `id_categoriaproduto`, `imagem`, `preco`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(219, 'DIREÇÃO HIDRAULICA', 0, 1, '0109201606264009795126.jpg', 0, '<div>\r\n	Estamparia em camisetas e abad&aacute;s para eventos, empresas, festas, etc. Com estampas na frente ou nas costas. Impress&atilde;o 100% digital, por m&eacute;todo de sublima&ccedil;&atilde;o, no pr&oacute;prio tecido.</div>\r\n<div>\r\n	Camisetas de todos os tamanhos.</div>\r\n<div>\r\n	Produto feito somente sob encomenda.</div>', '', '', '', 'SIM', 18, 'direcao-hidraulica');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `id_marcaservico` int(10) UNSIGNED NOT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(45) NOT NULL DEFAULT 'produto.jpg',
  `preco` double NOT NULL,
  `descricao` longtext NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `id_marcaservico`, `id_categoriaservico`, `imagem`, `preco`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(219, 'Serviço de Cromagem', 0, 5, '1009201610002750734289.jpg', 0, '<h4 style="text-align: justify;">\r\n	Processo utilizado para recobrir uma superf&iacute;cie met&aacute;lica (pe&ccedil;a) com cromo. As camadas de cromo duro s&atilde;o produzidas por eletrodeposi&ccedil;&atilde;o de cromo met&aacute;lico a partir de solu&ccedil;&otilde;es eletrol&iacute;ticas especiais, sob condi&ccedil;&otilde;es controladas de temperatura, concentra&ccedil;&atilde;o e eletricidade.</h4>\r\n<h4 style="text-align: justify;">\r\n	&nbsp;</h4>\r\n<h4 style="text-align: justify;">\r\n	O resultado do revestimento de Cromo Duro &eacute; uma camada com excelentes propriedades de dureza, ades&atilde;o e resist&ecirc;ncia &agrave; corros&atilde;o e &agrave;s altas temperaturas.</h4>', '', '', '', 'SIM', 18, 'servico-de-cromagem'),
(220, 'Serviços de Brunimento', 0, 7, '0909201603166794486857.jpg', 0, '<div style="text-align: justify;">\r\n	Processo utilizado para a corre&ccedil;&atilde;o de deforma&ccedil;&otilde;es internas (dilata&ccedil;&otilde;es, ovaliza&ccedil;&otilde;es e ranhuras) nas camisas de cilindros por meio de usinagem por abras&atilde;o, proporcionando uma geometria precisa do furo e um bom acabamento superficial. O brunimento permite que se trabalhe com elevada velocidade de corte, mas com baixa press&atilde;o de contato, o que diminui a temperatura na opera&ccedil;&atilde;o, aumenta a qualidade e economiza &oacute;leo lubrificante.</div>\r\n<div style="text-align: justify;">\r\n	&nbsp;</div>\r\n<div style="text-align: justify;">\r\n	A HB sempre preocupada em oferecer melhores condi&ccedil;&otilde;es de trabalho aos seus colaboradores, e compromissada com a prote&ccedil;&atilde;o ao meio-ambiente investiu no tratamento do ru&iacute;do provocado pelo brunimento de pe&ccedil;as. Para tanto utiliza moderna tecnologia envolvendo, al&eacute;m do isolamento ac&uacute;stico da brunidora, o tratamento do ru&iacute;do por meio de revestimento ac&uacute;stico, atenuadores de ru&iacute;do e porta ac&uacute;stica.</div>', '', '', '', 'SIM', 18, 'servicos-de-brunimento'),
(221, 'Serviços de Usinagem', 0, 3, '0909201603229485383638.jpg', 0, '<div style="text-align: justify;">\r\n	Processo mec&acirc;nico de corre&ccedil;&atilde;o que permite a restaura&ccedil;&atilde;o de diversas pe&ccedil;as. A HB possui m&aacute;quinas, t&eacute;cnicos e t&eacute;cnicas capazes de restaurar componentes com alto padr&atilde;o de qualidade, atendendo as espec&iacute;fica&ccedil;&otilde;es (os par&acirc;metros) do fabricante.</div>', '', '', '', 'SIM', 17, 'servicos-de-usinagem'),
(240, 'Serviços de Retífica', 0, 9, '0909201603193021846603.jpg', 0, '<div>\r\n	Garante a parte mais refinada em acabamentos cil&iacute;ndricos, garantindo a perfeita restaura&ccedil;&atilde;o da pe&ccedil;a, pois a execu&ccedil;&atilde;o &eacute; monitorada por instrumentos que permitem a leitura das menores escalas da metrologia.</div>', '', '', '', 'SIM', 0, 'servicos-de-retifica'),
(241, 'Serviços de Lapidação', 0, 10, '0909201603261951952539.jpg', 0, '<p style="text-align: justify;">\r\n	Restaura&ccedil;&atilde;o do acabamento em superf&iacute;cies planas e convexas de componentes de bombas e de motores para que os mesmo tenham os rendimentos desejados de acordo com as especifica&ccedil;&otilde;es dos fabricantes.</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'servicos-de-lapidacao');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_categoria_servicos`
--
ALTER TABLE `tb_categoria_servicos`
  ADD PRIMARY KEY (`idcategoriaservico`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`idgaleriaproduto`),
  ADD KEY `FK_tb_galerias_produtos_1` (`id_produto`);

--
-- Indexes for table `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  ADD PRIMARY KEY (`idgaleriaservico`);

--
-- Indexes for table `tb_grupos_logins`
--
ALTER TABLE `tb_grupos_logins`
  ADD PRIMARY KEY (`idgrupologin`);

--
-- Indexes for table `tb_grupos_logins_tb_paginas`
--
ALTER TABLE `tb_grupos_logins_tb_paginas`
  ADD PRIMARY KEY (`id_grupologin`,`id_pagina`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_marcas_produtos`
--
ALTER TABLE `tb_marcas_produtos`
  ADD PRIMARY KEY (`idmarcaproduto`);

--
-- Indexes for table `tb_marcas_servicos`
--
ALTER TABLE `tb_marcas_servicos`
  ADD PRIMARY KEY (`idmarcaservico`);

--
-- Indexes for table `tb_modulos_paginas`
--
ALTER TABLE `tb_modulos_paginas`
  ADD PRIMARY KEY (`idmodulopagina`);

--
-- Indexes for table `tb_paginas`
--
ALTER TABLE `tb_paginas`
  ADD PRIMARY KEY (`idpagina`);

--
-- Indexes for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  ADD PRIMARY KEY (`idparceiro`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_categoria_servicos`
--
ALTER TABLE `tb_categoria_servicos`
  MODIFY `idcategoriaservico` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `idgaleriaproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT for table `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  MODIFY `idgaleriaservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_grupos_logins`
--
ALTER TABLE `tb_grupos_logins`
  MODIFY `idgrupologin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1954;
--
-- AUTO_INCREMENT for table `tb_marcas_produtos`
--
ALTER TABLE `tb_marcas_produtos`
  MODIFY `idmarcaproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_marcas_servicos`
--
ALTER TABLE `tb_marcas_servicos`
  MODIFY `idmarcaservico` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_modulos_paginas`
--
ALTER TABLE `tb_modulos_paginas`
  MODIFY `idmodulopagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tb_paginas`
--
ALTER TABLE `tb_paginas`
  MODIFY `idpagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  MODIFY `idparceiro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD CONSTRAINT `FK_tb_galerias_produtos_1` FOREIGN KEY (`id_produto`) REFERENCES `tb_produtos` (`idproduto`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
