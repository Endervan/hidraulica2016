﻿<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();

if(!empty($_REQUEST[get1]))
{
    // INTERNA DE CATEGORIA
    $url = Util::trata_dados_formulario($_REQUEST[get1]);
    if(!empty($url))
    {
        $result = $obj_site->select("tb_categorias_produtos","AND url_amigavel = '$url'");
        $row = mysql_fetch_array($result);
        $complemento = "AND id_categoriaproduto = $row[idcategoriaproduto]";
    }
}

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description($row[description_google]);
$keywords = $obj_site->get_keywords($row[keywords_google]);
$titulo_pagina = $obj_site->get_title($row[title_google]);
?>
<!doctype html>
<html>
<head>
    <?php include("../../includes/head.php"); ?>
</head>

<body>
    
    <?php include("../../includes/topo.php"); ?>
        
    <?php include("../../includes/banners_internas.php"); ?>
    
    <div class="clear">&nbsp;</div>
    
    <div id="conteudo" class="produtos">
        <?php include("../../includes/filtros_produto.php"); ?>
        <?php
        // VERIFICA SE ESTÁ NA INTERNA OU NA LISTAGEM DE CATEGORIAS
        if(!empty($_REQUEST[get1]))
        {
            include("../../includes/lista_produtos.php");
        }
        else
        {
            include("../../includes/lista_categorias.php");
        }
        ?>
        
        <div class="clear">&nbsp;</div>
    </div>
	
    <?php include("../../includes/rodape.php"); ?>
	
</body>
</html>
