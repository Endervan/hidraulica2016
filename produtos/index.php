﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description("");
$keywords = $obj_site->get_keywords("");
$titulo_pagina = $obj_site->get_title("");
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="produtos">
		
		<?php
		if(isset($_POST[busca]))
		{
			// INTERNA DE CATEGORIA
			$busca = Util::trata_dados_formulario($_POST[busca]);
			if(!empty($busca))
			{
				$complemento = "AND titulo like '%$busca%'";
			}
		}
		?>
		
		<?php include("../includes/filtros_produto.php"); ?>
		<?php include("../includes/lista_produtos.php"); ?>
		
		<div class="clear">&nbsp;</div>
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>
