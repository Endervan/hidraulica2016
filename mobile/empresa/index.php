<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>
<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners", "idbanner", 11) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 152px center  no-repeat;
}
</style>

<body class="bg-interna">
	<?php require_once('../includes/topo.php'); ?>





	<!-- descricao empresa -->
	<div class="container">
		<div class="row">
			<div class="col-xs-12 fundo-transparente-empresa">
				<h3>HIDRÁULICA BRASIL</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 descricao-empresa">
				<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>

				<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",450, 267, array("class"=>"input100 top15", "alt"=>"$row[titulo]")) ?>

				<h2><?php echo($row[descricao]) ?></h2>
			</div>
		</div>
	</div>
	<!-- descricao empresa -->





	<?php require_once('../includes/rodape.php'); ?>

</body>
</html>
