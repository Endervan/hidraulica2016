<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>
<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners", "idbanner", 12) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 152px center  no-repeat;
}
</style>

<body class="bg-interna">
	<?php require_once('../includes/topo.php'); ?>




		<div class="container top175">
			<div class="row titulo_geral">
				<!-- ======================================================================= -->
				<!-- TITULO CONTATOS    -->
				<!-- ======================================================================= -->
				<div class="col-xs-12">
	           <h1>NOSSOS SERVIÇOS</h1>
				</div>
				<!-- ======================================================================= -->
				<!-- TITULO CONTATOS    -->
				<!-- ======================================================================= -->
			</div>
		</div>

		<!-- ======================================================================= -->
	  <!-- DESCRICAO NOSSOS SERVICOS    -->
	  <!-- ======================================================================= -->
	  <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
	  <div class="container">
	      <div class="row">
	        <div class="col-xs-12">
	          <div class="top60">
	            <p>
	            <span>  <?php Util::imprime($row[descricao]); ?></span>
	            </p>
	            </div>
	        </div>
	      </div>
	  </div>
	  <!-- ======================================================================= -->
	  <!-- DESCRICAO NOSSOS SERVICOS    -->
	  <!-- ======================================================================= -->





		    <!-- ======================================================================= -->
		    <!-- SERVICOS HOME    -->
		    <!-- ======================================================================= -->
		    <div class="container">
		      <div class="row">

		        <?php
		        $result = $obj_site->select("tb_servicos","order by rand()");
		        if (mysql_num_rows($result) > 0) {
		          $i = 0;
		          while($row = mysql_fetch_array($result)){
		            ?>
		            <!-- ======================================================================= -->
		            <!-- item 01 -->
		            <!-- ======================================================================= -->

								<div class="col-xs-4 text-center top15 ">
									<div class="lista-parceiros pb15">

										<a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?> ">

											<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 118, 161, array("class"=>"top15 bottom10", "alt"=>"$row[titulo]") ) ?>
										</a>
										<div class="lista-parceiros-linha">
											<h1><?php Util::imprime($row[titulo]) ?></h1>
										</div>
								</div>
							</div>
		            <?php
		            if ($i == 2) {
		              echo '<div class="clearfix"></div>';
		              $i = 0;
		            }else{
		              $i++;
		            }
		          }
		        }
		        ?>
		        <!-- ======================================================================= -->
		        <!-- item 01 -->
		        <!-- ======================================================================= -->

		    </div>
				</div>
		    <!-- ======================================================================= -->
		    <!-- SERVICOS HOME    -->
		    <!-- ======================================================================= -->


	<!-- rodape -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- rodape -->

</body>
</html>
