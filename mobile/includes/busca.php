<div class="container">
    <div class="row top20">
        <div class="col-xs-12 ">
            <div class="input-group barra-pesquisa-dicas">
                <input type="text" class="form-control " placeholder="Encontre o Produto Desejado">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>

    <div class="col-xs-12 top5 menus-produtos text-right top20">
        <h3 class="text-center">Filtrar por:</h3>
    </div>



    <div class="col-xs-12">
        <div class="dropdown">
            <button class="btn btn-default btn-drop dropdown-toggle pull-right" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Categorias
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu drop-personalizado" aria-labelledby="dropdownMenu1">
                <li>
                    <!--lista de produtos geral-->
                    <div class="col-xs-6 text-center top20">
                        <div class="descricao-produtos-geral">
                            <a href="">
                                <h1>LAVADORA DE PISOS</h1>
                                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-geral.jpg" alt="">
                            </a>
                            <p>Enceradeira Lavadora Pisos Frios</p>

                            <h2>R$ 200,00</h2>

                            <!-- botao-azul -->
                            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn-primary btn-azul-home1" role="button">Ver Mais</a>
                            <!-- botao-azul <-->

                        </div>
                    </div>

                    <div class="col-xs-6 text-center top20">
                        <div class="descricao-produtos-geral">
                            <a href="">
                                <h1>LAVADORA DE PISOS</h1>
                                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-geral.jpg" alt="">
                            </a>
                            <p>Enceradeira Lavadora Pisos Frios</p>

                            <h2>R$ 200,00</h2>

                            <!-- botao-azul -->
                            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn-primary btn-azul-home1" role="button">Ver Mais</a>
                            <!-- botao-azul <-->

                        </div>
                    </div>


                    <!--lista de produtos geral-->
                </li>



            </ul>

        </div>
    </div>

</div>
