<div class="container">


  <!-- menu -->
  <div class="row">
    <div class="col-xs-12 barra bottom10">  </div>


    <div class="col-xs-4 text-center top5">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
      </a>
    </div>

    <!-- telefone -->
    <div class="col-xs-8 text-right">

      <div class="media pull-right top5">
        <div class="media-left">
          <h4 class="media-heading top5">
            <?php Util::imprime($config[estado1]) ?> -
            <?php Util::imprime($config[telefone1]) ?></h4>
        </div>
        <div class="media-body">
          <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>" class="btn btn-cinza media-object" title="CHAMAR">
            CHAMAR
          </a>
        </div>
      </div>

      <div class="media pull-right top5">
        <div class="media-left">
          <h4 class="media-heading top5">
            <?php Util::imprime($config[estado2]) ?> -
            <?php Util::imprime($config[telefone2]) ?></h4>
        </div>
        <div class="media-body">
          <a href="tel:+55<?php Util::imprime($config[telefone2]) ?>" class="btn btn-cinza media-object" title="CHAMAR">
            CHAMAR
          </a>
        </div>
      </div>
<?php /*
      <div class="media pull-right top5">
        <div class="media-left">
          <h4 class="media-heading top5">
            <?php Util::imprime($config[estado3]) ?> -
            <?php Util::imprime($config[telefone3]) ?></h4>
        </div>
        <div class="media-body">
          <a href="tel:+55<?php Util::imprime($config[telefone3]) ?>" class="btn btn-cinza media-object" title="CHAMAR">
            CHAMAR
          </a>
        </div>
      </div>
*/ ?>
    </div>
    <!-- telefone -->

    <div class="col-xs-12 fundo_topo_menu top25 padding0 fonte_lato">
      <select class="menu-home" id="menu-site">
        <div class="menu-home1"></div>
        <option value=""></option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile">HOME</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa/">EMPRESA</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/categorias/">PRODUTOS</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/servicos/">SERVIÇOS</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/parceiros/">PARCEIROS</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/contatos/">CONTATOS</option>
      </select>
    </div>
  </div>
  <!-- menu -->



</div>
