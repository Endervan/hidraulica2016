<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <body>



    <?php require_once('./includes/topo.php'); ?>



    <!-- slider -->
    <div class="container">
      <div class="row slider-index telefone-topo1">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators navs-personalizados">
            <?php
            $result = $obj_site->select("tb_banners", "and tipo_banner = 2 order by ordem limit 3");
            if(mysql_num_rows($result) > 0){
              $i = 0;
              while ($row = mysql_fetch_array($result)) {
                $imagens[] = $row;
                ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                <?php
                $i++;
              }
            }
            ?>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">

            <?php
            if (count($imagens) > 0) {
              $i = 0;
              foreach ($imagens as $key => $imagem) {
                ?>
                <div class="item <?php if($i == 0){ echo "active"; } ?>">

                  <?php if (!empty($imagem[url])): ?>
                    <a href="<?php Util::imprime($imagem[url]); ?>" >
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                    </a>
                  <?php else: ?>
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                  <?php endif; ?>

                </div>
                <?php
                $i++;
              }
            }
            ?>




          </div>


        </div>
      </div>
    </div>
    <!-- slider -->


    <div class='container '>
      <div class="row">
        <div class="col-xs-12 top30">
          <div class="servicos-home_mobile">
            <h5><i class="fa fa-star"></i>SERVIÇOS</h5>
          </div>
        </div>
        <!-- servicos -->
        <?php include("includes/slider_servicos.php"); ?>
        <!-- servicos -->

        <div class="col-xs-12 top25 text-center">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/ver_todos_servicos.png" alt="">
          </a>
        </div>


      </div>


    </div>



    <div class="container top30 bottom50">
      <div class="row">
        <!-- PRODUTOS HOME -->
        <div class="col-xs-6">
          <div class="produtos_home">
            <h5>PRODUTOS</h5>
          </div>

          <!-- descricao -->
          <?php
          $result = $obj_site->select("tb_produtos","order by rand() LIMIT 4");
          if(mysql_num_rows($result) > 0)
          {
            while($row = mysql_fetch_array($result))
            {
              ?>

              <div class="col-xs-12 padding0 produtos_desc_home top20">

                <div class="col-xs-5 padding0">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 93, 57, array('class'=>'')) ?>

                </div>
                <div class="col-xs-7">
                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                    <!-- <p><?php //Util::imprime($row[descricao], 180); ?></p> -->
                  </a>
                </div>

                <div class="col-xs-12 borda_produtos top10"></div>


              </div>

              <?php
              if ($i == 2) {
                echo '<div class="clearfix"></div>';
                $i = 0;
              }else{
                $i++;
              }
            }
          }
          ?>
          <!-- descricao -->

          <div class="clearfix">  </div>
          <div class="col-xs-12 padding0 text-right">
            <!-- botao-azul -->
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn-primary btn-azul-home" role="button">Ver Todos</a>
            <!-- botao-azul -->
          </div>
        </div>
        <!-- PRODUTOS HOME -->

        <!-- EMPRESA HOME -->
        <div class="col-xs-6">
          <div class="produtos_home">
            <h5>EMPRESA</h5>
          </div>

          <!-- descricao -->
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
          <div class="col-xs-12 padding0 empresa_desc_home top5">
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",210, 112, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>

            <div class="top10">
              <p>
                <?php Util::imprime($row[descricao],500); ?>
              </p>
            </div>

          </div>



          <div class="clearfix">  </div>
          <div class="col-xs-12 padding0 text-right">
            <!-- botao-azul -->
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa" class="btn btn-primary btn-azul-home" role="button">Ver Mais</a>
            <!-- botao-azul -->
          </div>
        </div>
        <!-- EMPRESA HOME -->

      </div>
    </div>





    <div class="container">
      <div class="row">
        <!-- PARCEIROS HOME -->
        <div class="col-xs-12">
          <div class="parceiros_home">
            <h5>PARCEIROS</h5>
          </div>
        </div>

        <?php
        $result = $obj_site->select("tb_parceiros","limit 8");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while($row = mysql_fetch_array($result)){
            ?>

            <!-- ======================================================================= -->
            <!-- itens home   -->
            <!-- ======================================================================= -->
            <div class="col-xs-3 top25">
              <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" class="input100" alt="" />
            </div>
            <!-- ======================================================================= -->
            <!-- itens home   -->
            <!-- ======================================================================= -->
            <?php
            if ($i == 3) {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }
          }
        }
        ?>

        <!-- PARCEIROS HOME -->

      </div>
    </div>




    <?php require_once('./includes/rodape.php'); ?>


  </body>

  </html>
