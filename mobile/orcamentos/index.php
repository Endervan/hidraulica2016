<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();



//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
        $id = $_GET[id];
        unset($_SESSION[solicitacoes_produtos][$id]);
        sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
        $id = $_GET[id];
        unset($_SESSION[solicitacoes_servicos][$id]);
        sort($_SESSION[solicitacoes_servicos]);
        break;
    }

}

?>
<!doctype html>
<html>
<head>
    <?php require_once('../includes/head.php'); ?>

</head>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners", "idbanner", 17) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 152px center  no-repeat;
}
</style>

<body class="bg-interna">
    <?php require_once('../includes/topo.php'); ?>

    <div class="container top175">
      <div class="row titulo_contatos">
        <!-- ======================================================================= -->
        <!-- TITULO CONTATOS    -->
        <!-- ======================================================================= -->
        <div class="col-xs-12">
             <h1>ORÇAMENTOS</h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO CONTATOS    -->
        <!-- ======================================================================= -->
      </div>
    </div>




    <!-- contatos -->
    <div class="container ">
        <div class="row bottom20 top40 ">

            <div class="col-xs-12">
                <div class="menu-contatos">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs text-center " role="tablist">
                        <li role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/mobile/contatos"  ><i class="fa fa-envelope"></i>FALE CONOSCO</a></li>
                        <li role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco"  ><i class="fa fa-child"></i>TRABALHE CONOSCO</a></li>
                        <li role="presentation" class="active"><a href="#messages" ><i class="fa fa-calculator"></i>ORÇAMENTOS</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- CONTATOS -->
        <div class="row">
            <div class=" col-xs-12 contatos-descricao">
                <div class="col-xs-6"><h3><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span><?php Util::imprime($config[telefone1]) ?></h3></div>
                <div class="col-xs-4">
                    <div class=" top10">
                        <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>">
                            <button type="submit" class="btn btn-cinza-contatos1" name="CHAMAR">
                                CHAMAR
                            </button>
                        </a>
                    </div>
                </div>

            </div>

            <?php if (!empty($config[telefone2])): ?>
                <div class=" col-xs-12 contatos-descricao">
                    <div class="col-xs-6"><h3><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span><?php Util::imprime($config[telefone2]) ?></h3></div>
                    <div class="col-xs-4">
                        <div class=" top10">
                            <a href="tel:+55<?php Util::imprime($config[telefone2]) ?>">
                                <button type="submit" class="btn btn-cinza-contatos1" name="CHAMAR">
                                    CHAMAR
                                </button>
                            </a>
                        </div>
                    </div>

                </div>
            <?php endif; ?>

            <?php if (!empty($config[telefone3])): ?>
                <div class=" col-xs-12 contatos-descricao">
                    <div class="col-xs-6"><h3><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span><?php Util::imprime($config[telefone3]) ?></h3></div>
                    <div class="col-xs-4">
                        <div class=" top10">
                            <a href="tel:+55<?php Util::imprime($config[telefone3]) ?>">
                                <button type="submit" class="btn btn-cinza-contatos1" name="CHAMAR">
                                    CHAMAR
                                </button>
                            </a>
                        </div>
                    </div>

                </div>
            <?php endif; ?>

        </div>
        <!-- CONTATOS -->

        <!-- ENDERENCO -->
        <div class="row">
            <div class=" contatos-descricao">
                <div class="col-xs-9">
                    <div class="col-xs-2 top25">
                        <span class="glyphicon glyphicon-home btn-lg" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-10 padding0">
                        <h3><?php Util::imprime($config[endereco1]) ?></h3>
                    </div>

                </div>
                <div class="col-xs-3">
                    <div class=" top10">
                        <a href="">
                            <button type="submit" class="btn btn-cinza-contatos1" name="CHAMAR">
                                MAPA
                            </button>
                        </a>
                    </div>
                </div>

                <?php if (!empty($config[endereco2])): ?>
                    <div class="col-xs-9">
                        <div class="col-xs-2 top25">
                            <span class="glyphicon glyphicon-home btn-lg" aria-hidden="true"></span>
                        </div>
                        <div class="col-xs-10 padding0">
                            <h3><?php Util::imprime($config[endereco2]) ?></h3>
                        </div>

                    </div>
                    <div class="col-xs-3">
                        <div class=" top10">
                            <a href="">
                                <button type="submit" class="btn btn-cinza-contatos1" name="CHAMAR">
                                    MAPA
                                </button>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>


                <?php if (!empty($config[endereco3])): ?>
                    <div class="col-xs-9">
                        <div class="col-xs-2 top25">
                            <span class="glyphicon glyphicon-home btn-lg" aria-hidden="true"></span>
                        </div>
                        <div class="col-xs-10 padding0">
                            <h3><?php Util::imprime($config[endereco3]) ?></h3>
                        </div>

                    </div>
                    <div class="col-xs-3">
                        <div class=" top10">
                            <a href="">
                                <button type="submit" class="btn btn-cinza-contatos1" name="CHAMAR">
                                    MAPA
                                </button>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
        <!-- ENDERENCO -->

        <!-- Tab panes -->
        <div class="tab-content">


            <!-- orcamentos -->
            <div role="tabpanel" class="tab-pane fade in active" id="messages">



                <?php
                //  VERIFICO SE E PARA ENVIAR O EMAIL
                if(isset($_POST[nome]))
                {
                    //  CADASTRO OS PRODUTOS SOLICITADOS
                    for($i=0; $i < count($_POST[qtd]); $i++)
                    {
                        $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

                        $mensagem_produtos .=   "
                                                <tr>
                                                    <td><p>". $_POST[qtd][$i] ."</p></td>
                                                    <td><p>". ($dados[titulo]) ."</p></td>
                                                </tr>
                                                ";
                    }

                  $texto_mensagem = "
                                      Nome: ".Util::trata_dados_formulario($_POST[nome])." <br />
                                      Email: ".Util::trata_dados_formulario($_POST[email])." <br />
                                      Telefone: ".Util::trata_dados_formulario($_POST[telefone])." <br />
                                      Empresa: ".Util::trata_dados_formulario($_POST[empresa])." <br />
                                      ". utf8_decode('Área').": ".Util::trata_dados_formulario($_POST[area])." <br />
                                      Cidade: ".Util::trata_dados_formulario($_POST[cidade])." <br />
                                      Estado: ".Util::trata_dados_formulario($_POST[estado])." <br />
                                      Mensagem: <br />
                                      ".$mensagem =  utf8_decode(nl2br( $_POST[mensagem]))."

                                      <br />
                                        <h2> Produtos selecionados:</h2> <br />

                                        <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                                        <tr>
                                        <td><h4>QTD</h4></td>
                                        <td><h4>". utf8_decode('DESCRIÇÃO')."</h4></td>
                                        </tr>
                                        $mensagem_produtos


                                      ";



                  Util::envia_email($config[email], "CONTATO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, utf8_encode($_POST[nome]), $email);
                  //Util::envia_email($config[email_copia], "CONTATO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
                  Util::script_msg("Obrigado por entrar em contato.");
                  unset($_POST);
                }
                ?>


                <form class="form-inline FormOrcamento top20 pbottom20" role="form" method="post" enctype="multipart/form-data">

                    <?php
                    if(count($_SESSION[solicitacoes_produtos]) > 0){
                        ?>
                        <table class="table top20 lista-itens-orcamento" width="100%">
                            <thead>
                                <tr>
                                    <th align="left">ITEM</th>
                                    <th align="left">DESCRIÇÃO</th>
                                    <th class="text-center">QUANTIDADE</th>
                                    <th class="text-center">REMOVER</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                                    $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                                    ?>
                                    <tr>
                                        <td><img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="48" width="48" alt=""></td>
                                        <td><?php Util::imprime($row[titulo]) ?></td>
                                        <td align="center">
                                            <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                                            <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                                        </td>
                                        <td align="center">
                                            <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>

                        <?php
                    }
                    ?>


                    <?php
                    if(count($_SESSION[solicitacoes_servicos]) > 0){
                        ?>
                        <tbody>
                            <?php
                            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
                                $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                                ?>
                                <tr>
                                    <td><img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="48" width="48" alt=""></td>
                                    <td><?php Util::imprime($row[titulo]) ?></td>
                                    <td align="center">
                                        <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                                        <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                                    </td>
                                    <td align="center">
                                        <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <?php
                    }
                    ?>
  </table>



                    <div class="row top50">
                        <div class="col-xs-6 form-group ">
                            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                            <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                        </div>
                        <div class="col-xs-6 form-group">
                            <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                            <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 top20 form-group">
                            <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                            <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                        </div>
                        <div class="col-xs-6 top20 form-group">
                            <label class="glyphicon glyphicon-home"> <span>Empresa</span></label>
                            <input type="text" name="empresa" class="form-control fundo-form input100" placeholder="">
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-6 top20 form-group">
                            <label class="glyphicon glyphicon-cog"> <span>Area de Atuação</span></label>
                            <input type="text" name="area" class="form-control fundo-form input100" placeholder="">
                        </div>

                        <div class="col-xs-6 top20 form-group">
                            <label class="glyphicon glyphicon-globe"> <span>Cidade</span></label>
                            <input type="text" name="cidade" class="form-control fundo-form input100" placeholder="">
                        </div>


                    </div>


                    <div class="row">

                        <div class="col-xs-6 top20 form-group">
                            <label class="glyphicon glyphicon-globe"><span>Estado</span></label>
                            <input type="text" name="estado" class="form-control fundo-form input100" placeholder="">
                        </div>
                    </div>




                    <div class="row">
                        <div class="col-xs-12 top20 form-group">
                            <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                            <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="text-right top30">
                        <button type="submit" class="btn btn-azul-contatos" name="btn_trabalhe_conosco">
                            ENVIAR<span class="glyphicon glyphicon-ok">
                            </button>
                        </div>

                    </form>


                </div>
                <!-- orcamentos -->

            </div>
            <!-- Tab panes -->
            <!-- mapa-geral -->
            <!--  <div class="mapa-contatos">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3837.6217181778134!2d-48.01083799999997!3d-15.876467000000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a326fb9b303ed%3A0x6fd1705b0f2fad84!2sAlkha!5e0!3m2!1spt-BR!2sbr!4v1442685372964" width="475" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div> -->
        <!-- mapa-geral -->


    </div>
    <!-- contatos -->


    <!-- rodape -->
    <?php require_once('../includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>

<script>
$(document).ready(function() {
    $('.FormContato').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {

                    },
                    emailAddress: {
                        message: 'Esse endereço de email não é válido'
                    }
                }
            },
            telefone: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            assunto: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            mensagem: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    });
});
</script>



<script>
$(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {

                    },
                    emailAddress: {
                        message: 'Esse endereço de email não é válido'
                    }
                }
            },
            telefone: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            assunto: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            escolaridade: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            cargo: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            area: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            curriculo: {
                validators: {
                    notEmpty: {
                        message: 'Por favor insira seu currículo'
                    },
                    file: {
                        extension: 'doc,docx,pdf,rtf',
                        type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                        maxSize: 5*1024*1024,   // 5 MB
                        message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                    }
                }
            },
            mensagem: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    });
});
</script>



<script>
$(document).ready(function() {
    $('.FormOrcamento').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {

                    },
                    emailAddress: {
                        message: 'Esse endereço de email não é válido'
                    }
                }
            },
            telefone: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            empresa: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            area: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            cidade: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            servico: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            estado: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            curriculo: {
                validators: {
                    notEmpty: {
                        message: 'Por favor insira seu currículo'
                    },
                    file: {
                        extension: 'doc,docx,pdf,rtf',
                        type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                        maxSize: 5*1024*1024,   // 5 MB
                        message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                    }
                }
            },
            mensagem: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    });
});
</script>
